<?php
/*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * This file contains the functions that format and return 
 * the title and the content in DocBook (XML) format.
 */

/** */
function doc_header()
{
  $header = '<?xml version="1.0"?>
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN"
                      "http://nwalsh.com/docbook/xml/3.1.4/db3xml.dtd">
<book>
';
  return $header;
}

function doc_footer()
{
  $footer = "</book>";
  return $footer;
}

function node_header($is_article, $node_level)
{
  if ($is_article)          $tag = "<simplesect role='article'>";
  else if ($node_level==1)  $tag = "<chapter>";
  else                      $tag = "<sect".($node_level - 1).">";

  return $tag."\n";
}

function node_footer($is_article, $node_level)
{
  if ($is_article)          $tag = "</simplesect>";
  else if ($node_level==1)  $tag = "</chapter>";
  else                      $tag = "</sect".($node_level - 1).">";

  return $tag."\n";
}

function format_title($title, $is_article, $node_level)
{
  //if ($is_article)  return "";
  $title = replace_chars($title);
  $title = "<title>$title</title>";
  $title .= "\n";
  return $title;
}

function format_content(&$lines, $is_article, $node_level)
{
  if ($is_article)
    {
      $content = format_article($lines);
    }
  else 
    {
      $lines[] = ""; //add a sentinel line
      $paragraph = "";
      for ($i=0; $i < sizeof($lines); $i++)
        {
          $line = $lines[$i];
          if (trim($line)=="")
            {
              if ($paragraph=="")  continue;
              $paragraph = replace_chars($paragraph);
              $paragraph = "<para>$paragraph</para>";
              $content .= "\n".word_chunk_split($paragraph, 75, "\n")."\n";
              $paragraph = "";
            }
          else $paragraph .= $line." ";
        }
    }

  $content .= "\n";
  return $content;
}

function replace_chars($str)
{
  switch (LNG)
    {
    case "al":
      $e = chr(235);  //Ã«
      $E = chr(203);  //Ã
      $c = chr(231);  //Ã§
      $C = chr(199);  //C,
      $str = str_replace($e, '&euml;', $str);
      $str = str_replace($E, '&Euml;', $str);
      $str = str_replace($c, '&ccedil;', $str);
      $str = str_replace($C, '&Ccedil;', $str);
      break;

    case "it":
      $e = chr(233);
      $E = chr(201);
      $str = str_replace($e, "&eacute;", $str);
      $str = str_replace($E, "&Eacute;", $str);
      break;
    }

  return $str;
}

function format_article(&$lines)
{
  $arr = get_content_structure($lines);
  $xml_article = "";
  for ($i=0; $i < sizeof($arr); $i++)
    {
      $item = $arr[$i];
      if (!is_array($item))
        {
          $item = replace_chars($item);
          $item = "<para>$item</para>";
          $item = word_chunk_split($item, 70, "\n    ");
          $xml_article .= "    $item\n";
        }
      else
        {
          $xml_article .= format_list($item);
        }
    }

  return $xml_article;
}

function format_list(&$list)
{
  $xml_list = "  <orderedlist>\n";
  for ($i=0; $i < sizeof($list); $i++)
    {
      $nr = $list[$i][0];
      $item = $list[$i][1];
      $sublist = $list[$i][2];
      $item = replace_chars($item);
      $item = "<para>$item</para>";
      $item = word_chunk_split($item, 65, "\n     ");
      $xml_list .= "    <listitem>\n"
        . "     $item\n";
      if (sizeof($sublist)<>0)  $xml_list .= format_sublist($sublist);
      $xml_list .= "    </listitem>\n";
    }
  $xml_list .= "  </orderedlist>\n";
  return $xml_list;
}

function format_sublist(&$sublist)
{
  $xml_sublist = "      <orderedlist numeration='loweralpha'>\n";
  while (list($label,$item) = each($sublist))
    {
      $label = replace_chars($label);
      $item = replace_chars($item);
      $item = "<para>$item</para>";
      $item = word_chunk_split($item, 60, "\n          ");
      $xml_sublist .= "        <listitem>\n"
        . "          $item\n"
        . "        </listitem>\n";
    }
  $xml_sublist .= "      </orderedlist>\n";
  return $xml_sublist;
}
?>