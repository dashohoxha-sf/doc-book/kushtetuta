<?php
/*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

//get arguments format and lng
$browser = true;
$get_keys = array_keys($_GET);
$args = $get_keys[0];
$pos = strpos($args, '+');
if ($pos)
{
  $browser = false;
  $args = substr($args, $pos+1);
}
$arr_args = explode("/", $args);
$format = $arr_args[0];
$lng = (isset($arr_args[1]) ? $arr_args[1] : "al");

define("LNG", $lng);
define("FORMAT", $format);
define("UNDEFINED", "undefined");

//define content path
$path = dirname(__FILE__);
$path = str_replace("format", "", $path);
define("CONTENT_PATH", $path."content/");

$file_format_funcs = FORMAT."_format_funcs.php";
include $file_format_funcs;
include "folder_funcs.php";
include "content_structure.php";
include "chunk_split.php";

if ($browser)  print "<xmp>\n";
print_node();
if ($browser)  print "</xmp>\n";

exit(0);

/* ------------------------------------------------ */

function print_node($node_id ="", $ident ="")
{
  //get $title and $is_article
  $is_article = true;
  $title = get_article_title($node_id);
  if ($title==UNDEFINED)
    {
      $title = get_node_title($node_id);
      if ($title==UNDEFINED)  return;
      $is_article = false;
    }

  //get $node_level
  $arr = explode("/", $node_id);
  $node_level = sizeof($arr) - 1;

  //get formated title
  $title = format_title($title, $is_article, $node_level);

  //get formated content
  $content_lines = get_lines($node_id, "content");
  if (sizeof($content_lines)==0)
    {
      $content = "";
    }
  else
    {
      $content = format_content($content_lines, $is_article, $node_level);
    }

  //print node
  if ($node_id=="")  print doc_header(); 
  else print node_header($is_article, $node_level);

  if ($node_id<>"")  print $title;
  if ($content<>"")  print $content;
  
  //get subnodes and print them recursivly
  $subfolders = get_subfolders($node_id);
  for ($i=0; $i < sizeof($subfolders); $i++)
    {
      $subfolder = $subfolders[$i];
      $subnode_id = $node_id.$subfolder."/";
      print_node($subnode_id, $ident."    ");
    }

  if ($node_id=="")  print doc_footer(); 
  else print node_footer($is_article, $node_level);
}
?>