<?php
/*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * This file contains the functions that format and return 
 * the title and the content in latex (tex) format.
 */

/** */
function doc_header()
{
  $header = "
<html>
<head>
  <title>Kushtetuta e Shqiperise</title>
  <style>
    body  
    { 
      font-family: Helvetica, Arial;
      color: #113322;
    }

    H1 
    {
      width: 100%;
      background: #ff6666;
      border: 1px solid #cc0000;
      color: #000000; 
      text-align: center; 
      padding: 25 0; 
    }

    H2 
    {
      color: #aa0000;
      width: 100%;
      background: #bbffee;
      border: 1px solid #aaccee;
    }

    H3
    {
      color: #aa6600;
    }

    H4
    {
      color: #dd0044;
    }
  </style>
</head>
<body>
";
  return $header;
}

function doc_footer()
{
  $footer = "</body>\n</html>";
  return $footer;
}

function node_header($is_article, $node_level)
{
  return "";
}

function node_footer($is_article, $node_level)
{
  return "";
}

function format_title($title, $is_article, $node_level)
{
  $title = replace_chars($title);
  if ($is_article)
    {
      $title = "<h4>".$title."</h4>";
    }
  else
    {
      switch ($node_level)
        {
        case 1:
          $title = "<h1>".$title."</h1>";
          break;
        case 2:
          $title = "<h2>".$title."</h2>";
          break;
        case 3:
          $title = "<h3>".$title."</h3>";
          break;
        }
    }

  $title .= "\n";
  return $title;
}

function format_content(&$lines, $is_article, $node_level)
{
  if ($is_article)
    {
      $content = format_article($lines);
    }
  else 
    {
      $lines[] = ""; //add a sentinel line
      $paragraph = "";
      for ($i=0; $i < sizeof($lines); $i++)
        {
          $line = $lines[$i];
          if (trim($line)=="")
            {
              if ($paragraph=="")  continue;
              $paragraph = replace_chars($paragraph);
              $content .= "<p>\n"
                . word_chunk_split($paragraph, 75, "\n")."\n"
                . "</p>\n\n";
              $paragraph = "";
            }
          else $paragraph .= $line." ";
        }
    }

  $content .= "\n";
  return $content;
}

function replace_chars($str)
{
  switch (LNG)
    {
    case "al":
      $e = chr(235);  //Ã«
      $E = chr(203);  //Ã
      $c = chr(231);  //Ã§
      $C = chr(199);  //C,
      $str = str_replace($e, '&euml;', $str);
      $str = str_replace($E, '&Euml;', $str);
      $str = str_replace($c, '&ccedil;', $str);
      $str = str_replace($C, '&Ccedil;', $str);
      break;

    case "it":
      $e = chr(233);
      $E = chr(201);
      $str = str_replace($e, "&eacute;", $str);
      $str = str_replace($E, "&Eacute;", $str);
      $a = chr(224);
      $e = chr(232);
      $i = chr(236);
      $o = chr(242);
      $u = chr(249);
      $str = str_replace($a, "&agrave;", $str);
      $str = str_replace($e, "&egrave;", $str);
      $str = str_replace($i, "&igrave;", $str);
      $str = str_replace($o, "&ograve;", $str);
      $str = str_replace($u, "&ugrave;", $str);
      $A = chr(192);
      $E = chr(200);
      $I = chr(204);
      $O = chr(210);
      $U = chr(217);
      $str = str_replace($A, "&Agrave;", $str);
      $str = str_replace($E, "&Egrave;", $str);
      $str = str_replace($I, "&Igrave;", $str);
      $str = str_replace($O, "&Ograve;", $str);
      $str = str_replace($U, "&Ugrave;", $str);
      break;
    }

  return $str;
}

function format_article(&$lines)
{
  $arr = get_content_structure($lines);
  $tex_article = "";
  for ($i=0; $i < sizeof($arr); $i++)
    {
      $item = $arr[$i];
      if (!is_array($item))
        {
          $item = replace_chars($item);
          $item = word_chunk_split($item, 70, "\n    ");
          $tex_article .= "  <p>\n    ".$item."\n  </p>\n";
        }
      else
        {
          $tex_article .= format_list($item);
        }
    }

  return $tex_article;
}

function format_list(&$list)
{
  $tex_list = "  <ol>\n";
  for ($i=0; $i < sizeof($list); $i++)
    {
      $nr = $list[$i][0];
      $item = $list[$i][1];
      $sublist = $list[$i][2];
      $item = replace_chars($item);
      $item = word_chunk_split($item, 65, "\n     ");
      $tex_list .= "    <li>\n"
        . "     $item <br><br>\n";
      if (sizeof($sublist)<>0)  $tex_list .= format_sublist($sublist);
      $tex_list .= "    </li>\n";
    }
  $tex_list .= "  </ol>\n";
  return $tex_list;
}

function format_sublist(&$sublist)
{
  $tex_sublist = "      <ol type='a'>\n";
  while (list($label,$item) = each($sublist))
    {
      $label = replace_chars($label);
      $item = replace_chars($item);
      $item = word_chunk_split($item, 60, "\n          ");
      $tex_sublist .= "        <li>\n"
        . "          $item <br><br>\n"
        . "        </li>\n";
    }
  $tex_sublist .= "      </ol>\n";
  return $tex_sublist;
}
?>