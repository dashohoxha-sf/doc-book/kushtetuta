<?php
/*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * This file contains the functions that format and return 
 * the title and the content in latex (tex) format.
 */

/** */
function doc_header()
{
  $header = "
\documentclass[a4paper,12pt]{article}
\begin{document}

";
  return $header;
}

function doc_footer()
{
  $footer = "\\end{document}\n";
  return $footer;
}

function node_header($is_article, $node_level)
{
  return "";
}

function node_footer($is_article, $node_level)
{
  return "";
}

function format_title($title, $is_article, $node_level)
{
  $title = replace_chars($title);
  if ($is_article)
    {
      $title = "\\subsubsection*{".$title."}";
    }
  else
    {
      switch ($node_level)
        {
        case 1:
          $title = "\\section*{".$title."}";
          break;
        case 2:
          $title = "\\section{".$title."}";
          break;
        case 3:
          $title = "\\subsection{".$title."}";
          break;
        }
    }

  $title .= "\n\n";
  return $title;
}

function format_content(&$lines, $is_article, $node_level)
{
  if ($is_article)
    {
      $content = format_article($lines);
    }
  else 
    {
      $lines[] = ""; //add a sentinel line
      $paragraph = "";
      for ($i=0; $i < sizeof($lines); $i++)
        {
          $line = $lines[$i];
          if (trim($line)=="")
            {
              if ($paragraph=="")  continue;
              $paragraph = replace_chars($paragraph);
              $content .= word_chunk_split($paragraph, 75, "\n")."\n\n";
              $paragraph = "";
            }
          else $paragraph .= $line." ";
        }
    }

  $content .= "\n";
  return $content;
}

function replace_chars($str)
{
  switch (LNG)
    {
    case "al":
      $e = chr(235);  //Ã«
      $E = chr(203);  //Ã
      $c = chr(231);  //Ã§
      $C = chr(199);  //C,
      $str = str_replace($e, '\\"{e}', $str);
      $str = str_replace($E, '\\"{E}', $str);
      $str = str_replace($c, '\\c{c}', $str);
      $str = str_replace($C, '\\c{C}', $str);
      break;

    case "it":
      $e = chr(233);
      $E = chr(201);
      $str = str_replace($e, "\\'{e}", $str);
      $str = str_replace($E, "\\'{E}", $str);
      $a = chr(224);
      $e = chr(232);
      $i = chr(236);
      $o = chr(242);
      $u = chr(249);
      $str = str_replace($a, "\\`{a}", $str);
      $str = str_replace($e, "\\`{e}", $str);
      $str = str_replace($i, "\\`{i}", $str);
      $str = str_replace($o, "\\`{o}", $str);
      $str = str_replace($u, "\\`{u}", $str);
      $A = chr(192);
      $E = chr(200);
      $I = chr(204);
      $O = chr(210);
      $U = chr(217);
      $str = str_replace($A, "\\`{A}", $str);
      $str = str_replace($E, "\\`{E}", $str);
      $str = str_replace($I, "\\`{I}", $str);
      $str = str_replace($O, "\\`{O}", $str);
      $str = str_replace($U, "\\`{U}", $str);
      break;
    }

  return $str;
}

function format_article(&$lines)
{
  $arr = get_content_structure($lines);
  $tex_article = "";
  for ($i=0; $i < sizeof($arr); $i++)
    {
      $item = $arr[$i];
      if (!is_array($item))
        {
          $item = replace_chars($item);
          $item = word_chunk_split($item, 70, "\n    ");
          $tex_article .= "    ".$item."\n\n";
        }
      else
        {
          $tex_article .= format_list($item);
        }
    }

  return $tex_article;
}

function format_list(&$list)
{
  $tex_list = "  \\begin{enumerate}\n";
  for ($i=0; $i < sizeof($list); $i++)
    {
      $nr = $list[$i][0];
      $item = $list[$i][1];
      $sublist = $list[$i][2];
      $item = replace_chars($item);
      $item = word_chunk_split($item, 65, "\n       ");
      $tex_list .= "    \\item\n       $item\n";
      if (sizeof($sublist)<>0)  $tex_list .= format_sublist($sublist);
    }
  $tex_list .= "  \\end{enumerate}\n";
  return $tex_list;
}

function format_sublist(&$sublist)
{
  $tex_sublist = "      \\begin{description}\n";
  while (list($label,$item) = each($sublist))
    {
      $label = replace_chars($label);
      $item = replace_chars($item);
      $item = word_chunk_split($item, 60, "\n             ");
      $tex_sublist .= "        \\item[$label])\n             $item\n";
    }
  $tex_sublist .= "      \\end{description}\n";
  return $tex_sublist;
}
?>