#!/bin/bash

STYLESHEETS=/usr/share/sgml/docbook/dsssl-stylesheets
FORMATS=../formats
XML_FILE=$FORMATS/kushtetuta_$2.xml

case "${1}" in
  html )
    docbook2html -o $FORMATS/html_${2}_1  $XML_FILE
    exit 0
    ;;
  rtf )
    docbook2rtf -o $FORMATS  $XML_FILE
    exit 0
    ;;
  txt )
    docbook2txt -o $FORMATS/1/ $XML_FILE
    exit 0
    ;;
  * )
    echo "Usage: ${0}  [ html | rtf | txt ] [ al | it ]"
         exit 0
         ;;
esac

