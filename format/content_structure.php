<?php
/*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * This function gets as input an array of lines without eoln 
 * (the lines of a content file), and returns a more organized
 * structure.  This structure contains lines (paragraphs), lists
 * and sublists.  It is an array containing strings and lists;
 * each list is an array that contains an array for each item of
 * the list.  The array of the item contains the number of the
 * item, the string of the item, and array of the sublist (which
 * may beempty).  The array of the sublist is an associated array
 * with key the letter of the sublist and value the string of the
 * sublist.
 * 
 * Example: Supose that the content of the $lines, as read from
 * the file, is this:
 * $lines = array(
 *      "line 1",
 *      "line 2",
 *      "",
 *      "line 3",
 *      "",
 *      "  1. item1",
 *      "     xxx",
 *      "",
 *      "  2. item2",
 *      "",
 *      "     a) subitem1",
 *      "",
 *      "     b) subitem2",
 *      "        yyy",
 *      "  3. item3",
 *      "",
 *      "line 4"
 *      );
 *
 * After calling get_content_structure($lines), we get the following 
 * array:
 * $content_structure = 
 * array(
 *     "line 1 line 2",
 *     "line 3",
 *     array(
 *      array("1", "item1 xxx", array()),
 *      array("2", "item2", array(
 *                    "a" => "subitem1", 
 *                    "b" => "subitem2 yyy"
 *                    )),
 *      array("3", "item3", array())
 *      ),
 *     "line 4"
 *     )
 *
 * As can be seen, it is more compact and can be converted easily 
 * to other formats.
 */

function get_content_structure(&$lines)
{
  $content_structure = array();
  $list = array();
  $sublist = array();
  $i = 0;
  $nr_lines = sizeof($lines);
  while ($i < $nr_lines)
    {
      $line = trim($lines[$i]);
      //skip any empty lines, until we get a non-empty line
      while ($line=="")
        {
          $i++;
          if ($i >= $nr_lines)  break 2;
          $line = trim($lines[$i]);
        }

      if (ereg("^([[:digit:]]+)\\.", $line, $regs))
        {
          if (sizeof($sublist)<>0)
            {
              $idx = sizeof($list) - 1;
              $list[$idx][2] = $sublist;
              $sublist = array();
            }

          $nr = $regs[1];
          $line = ereg_replace("^".$nr."\\. *", '', $line);
          //read all the lines until the next empty line
          $item = "";
          while ($line<>"")
            {
              $item .= $line." ";
              $i++;
              if ($i >= $nr_lines)  break;
              $line = trim($lines[$i]);
            }
          $list[] = array($nr, $item, array());
        }
      else if (ereg("^(..?)\\)", $line, $regs))
        {
          $char = $regs[1];
          $line = ereg_replace("^".$char."\\) *", '', $line);
          //read all the lines until the next empty line
          $item = "";
          while ($line<>"")
            {
              $item .= $line." ";
              $i++;
              if ($i >= $nr_lines)  break;
              $line = trim($lines[$i]);
            }
          $sublist["$char"] = $item;
        }
      else 
        {
          if (sizeof($sublist)<>0)
            {
              $idx = sizeof($list) - 1;
              $list[$idx][2] = $sublist;
              $sublist = array();
            }
          if (sizeof($list)<>0)
            {
              $content_structure[] = $list;
              $list = array();
            }
          //read all the lines until the next empty line
          $item = "";
          while ($line<>"")
            {
              $item .= $line." ";
              $i++;
              if ($i >= $nr_lines)  break;
              $line = trim($lines[$i]);
            }
          $content_structure[] = $item;
        }
    }

  if (sizeof($sublist)<>0)
    {
      $idx = sizeof($list) - 1;
      $list[$idx][2] = $sublist;
    }
  if (sizeof($list)<>0)
    {
      $content_structure[] = $list;
    }

  return $content_structure;
}

/* --- print the content structure in txt format, for debug --- */

function print_structure($arr)
{
  print "<pre>\n";
  print "\n----------------\n";
  for ($i=0; $i < sizeof($arr); $i++)
    {
      $item = $arr[$i];
      if (!is_array($item))
        {
          print $item."\n\n";
        }
      else
        {
          print_list($item);
        }
    }
  print "</pre>\n";
  print "\n----------------\n";
}

function print_list($list)
{
  for ($i=0; $i < sizeof($list); $i++)
    {
      $nr = $list[$i][0];
      $li = $list[$i][1];
      $sublist = $list[$i][2];
      print "    $nr. $li\n\n";
      print_sublist($sublist);
    }
}

function print_sublist($sublist)
{
  if (sizeof($sublist)==0)  return;
  while (list($char,$li) = each($sublist))
    {
      print "        $char) $li \n\n";
    }
}
?>