<?php
/*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * This file contains the functions that format and return 
 * the title and the content in text (txt) format.
 */

/** */
function doc_header()
{
  return "";
}

function doc_footer()
{
  return "";
}

function node_header($is_article, $node_level)
{
  return "";
}

function node_footer($is_article, $node_level)
{
  return "";
}

function format_title($title, $is_article, $node_level)
{
  if ($is_article)  $title .= ":";
  $length = strlen($title);
  $title .= "\n";
  $underline = ($is_article ? "-" : "=");
  for ($i=0; $i < $length; $i++)  $title .= $underline;
  $title .= "\n";
  return $title;
}

function format_content($lines, $is_article, $node_level)
{
  if ($is_article)
    {
      $content = format_article($lines);
    }
  else 
    {
      $lines[] = ""; //add a sentinel line
      $paragraph = "";
      for ($i=0; $i < sizeof($lines); $i++)
        {
          $line = $lines[$i];
          if (trim($line)=="")
            {
              if ($paragraph=="")  continue;
              $content .= word_chunk_split($paragraph, 75, "\n")."\n\n";
              $paragraph = "";
            }
          else $paragraph .= $line." ";
        }
    }

  $content .= "\n";
  return $content;
}

function format_article(&$lines)
{
  $arr = get_content_structure($lines);
  $txt_article = "";
  for ($i=0; $i < sizeof($arr); $i++)
    {
      $item = $arr[$i];
      if (!is_array($item))
        {
          $item = word_chunk_split($item, 65, "\n    ");
          $txt_article .= "    ".$item."\n\n";
        }
      else
        {
          $txt_article .= format_list($item);
        }
    }

  return $txt_article;
}

function format_list(&$list)
{
  $txt_list = "";
  for ($i=0; $i < sizeof($list); $i++)
    {
      $nr = $list[$i][0];
      $item = $list[$i][1];
      $sublist = $list[$i][2];
      $item = word_chunk_split($item, 65, "\n       ");
      $txt_list .= "    $nr. $item\n\n";
      if (sizeof($sublist)<>0)  $txt_list .= format_sublist($sublist);
    }
  return $txt_list;
}

function format_sublist(&$sublist)
{
  $txt_sublist = "";
  while (list($label,$item) = each($sublist))
    {
      $item = word_chunk_split($item, 60, "\n           ");
      $txt_sublist .= "        $label) $item \n\n";
    }
  return $txt_sublist;
}
?>