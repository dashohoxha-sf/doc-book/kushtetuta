#!/bin/bash

STYLESHEETS=/usr/share/sgml/docbook/xsl-stylesheets
FORMATS=../formats
XML_FILE=$FORMATS/kushtetuta_$2.xml

case "${1}" in
  html )
     xmlto html -v -o $FORMATS/html_$2 $XML_FILE
     exit 0
     ;;
  #rtf )
  #   xmlto rtf -v -o $FORMATS/1/ $XML_FILE
  #   exit 0
  #   ;;
  #pdf )
  #   xmlto pdf -v -o $FORMATS/1/ $XML_FILE
  #   exit 0
  #   ;;
  * )
     echo "Usage: ${0}  [ html | rtf | pdf ]  [ al | it ]"
     exit 0
     ;;
esac

