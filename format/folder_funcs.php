<?php
/*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * Functions that get data from the folders and files 
 * in which is stored the content.
 */

/**
 * Get the title of the given node, 
 * according to the selected language.
 */
function get_node_title($node_id)
{
  $folder_path = CONTENT_PATH.$node_id;
  $title_file = $folder_path."title_".LNG.".txt";
  if (file_exists($title_file))
    {
      $arr_file = file($title_file);
      $title = trim($arr_file[0]);
    }
  else
    {
      $title = UNDEFINED;
    }

  return $title;
}

/** Returns the title of an article */
function get_article_title($node_id)
{
  if (!ereg("/neni-", $node_id))  return UNDEFINED;
  
  $art_nr = substr($node_id, -4, 3) + 0;
  switch (LNG)
    {
    default:
    case "al":
      $artic = "Neni";
      break;
    case "en":
      $artic = "Article";
      break;
    case "it":
      $artic = "Articolo";
      break;
    }

  return $artic." ".$art_nr;
}

/**
 * Returns the array of the content lines of the 
 * given node, according to the selected language.
 * $filetype is 'content', 'comments', etc. 
 */
function get_lines($node_id, $filetype)
{
  $folder_path = CONTENT_PATH.$node_id;
  $filename = $folder_path.$filetype."_".LNG.".txt";
  if (!file_exists($filename))  return array();

  $arr_lines = file($filename);
  for ($i=0; $i < sizeof($arr_lines); $i++)
    {
      $line = $arr_lines[$i];
      $arr_lines[$i] = chop($line);
    }

  return $arr_lines;
}

/**
 * Returns an array of the subfolders of the given folder. 
 */
function get_subfolders($node_id)
{
  $subfolders = array();

  //open the folder
  $folder = CONTENT_PATH.$node_id;
  $dir = opendir($folder);
  if (!$dir)
    {
      print "get_subfolders(): Cannot open folder '$folder'."; 
      return $subfolders;
    }

  //get subfolders
  while ($fname = readdir($dir))
    {
      if (filetype($folder.$fname) == 'dir')
        {
          if ($fname=="." or $fname==".." or $fname=="CVS")  continue;
          $subfolders[] = $fname;
        }
    }
  closedir($dir);

  sort($subfolders);
  return $subfolders;
}
?>