<?php
/*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/** 
 * Splits the given line into lines of less then 70 chars long
 * and returns them as a string. Different from the built-in
 * 'chunk_split()' function of PHP, this function does not 
 * break words in the middle.
 */
function word_chunk_split($long_line, $chunklen =70, $separator ="\n")
{
  $words = explode(" ", $long_line);
  $text = "";
  $short_line = "";
  for ($i=0; $i < sizeof($words); $i++)
    {
      $word = $words[$i];
      if ((strlen($short_line) + strlen($word)) > $chunklen)
        {
          $text .= $short_line.$separator;
          $short_line = "";
        }
      $short_line .= $word." ";
    }
  if ($short_line <> "")  $text .= $short_line;

  return $text;
}
?>