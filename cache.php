<?php
/*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

$app_path = dirname(__FILE__);
$app_path = str_replace("\\", "/", $app_path);  //could be a Windows path
define("APP_PATH",      $app_path."/");

define("CONFIG_PATH",   APP_PATH."config/");
include_once CONFIG_PATH."const.Paths.php";

//include configuration constants
include_once CONFIG_PATH."const.Options.php";

//paths of the application framework
include_once WEBAPP_PATH."const.Paths.php";

//include session
include_once SESSION_PATH."class.Request.php";
include_once SESSION_PATH."class.Event.php";
include_once SESSION_PATH."class.Session.php";

//declare constants
define("UNDEFINED", "undefined");

//declare some global variables
$request = new Request; 
$event   = new Event;
$session = new Session;

$cache_file = get_cache_file();
if (USE_CACHE) 
{
  //if not in EDIT mode and we have a cache file, try to use the cache file
  if (!defined(EDIT) and $cache_file<>UNDEFINED)
    {
      //if cache file exists, return the cache file and exit
      if (@readfile(CACHE_PATH.$cache_file))   exit;
    }
}

/*--------------------------------------------------*/

/** returns the cache file */
function get_cache_file()
{
  global $event, $session;

  if ($event->targetPage==UNDEFINED)
    { 
      //first time that the user enters in the application
      return "content_al.html";
    }
  
  //the search page cannot be cached and does not have a cache file
  if ($event->target=="contentbox")
    {
      if ($event->name=="display_search")   return UNDEFINED;
    }
  else
    {
      $content_file = $session->Vars["contentbox->file"];
      if ($content_file=="search/search.html")  return UNDEFINED;
    }
  
  //the name of the cache file will be composed of node_id and lng
  $node_id = $session->Vars["content->selected"];
  $lng = $session->Vars["langs->selected"];

  //some events change node_id and lng
  if ($event->target=="content" and $event->name=="select")
    {
      $node_id = $event->args["node"];
    }
  if ($event->target=="contentbox" and $event->name=="display_content")
    {
      $node_id = $event->args["node_id"];
    }
  if ($event->target=="langs" and $event->name=="select")
    {
      $lng = $event->args["lng"];
    }

  //construct and return the cache file
  $cache_file = $node_id."content_".$lng.".html";
  return $cache_file;
}

function write_cache($cache_file, &$html_file)
{
  if (defined(EDIT))    return;  //don't cache when in edit mode
  if ($cache_file=="")  return;  //no cache file

  //create the folders and subfolders, if they do not exist
  $path = "";
  while (ereg("^([^/]*/)", $cache_file, $regs))
    {
      $path .= $regs[1];
      $dir = CACHE_PATH.$path;
      if (!file_exists($dir))   mkdir($dir, 0775);
      $cache_file = ereg_replace("^".$regs[1], "", $cache_file);
      //print $path."<br>\n";
    }

  //write the page to the cache file
  $fp = fopen(CACHE_PATH.$path.$cache_file, "w");
  fputs($fp, $html_file);
  fclose($fp);
}
?>