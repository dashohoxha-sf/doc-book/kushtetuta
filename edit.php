<?php
/*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

if(!isset($_SERVER["PHP_AUTH_USER"])) 
{
  header("WWW-Authenticate: Basic realm=\"Editors\"");
  header("HTTP/1.0 401 Unauthorized");
  //header("Location: /kushtetuta/");
  echo "Sorry, this page cannot be accessed by everybody!\n";
  exit;
}
else
{
  $user = $_SERVER["PHP_AUTH_USER"];
  $passwd = $_SERVER["PHP_AUTH_PW"];
  //validate user and password
  //. . . . . . . .

  define("EDIT", "true");
  include "index.php";
}
?>
