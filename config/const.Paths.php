<?php
//constants of the paths in the application
define("WEBAPP_PATH",		UP_PATH."sf/web_app/");
define("CONTENT_PATH",		"/var/www/html/kushtetuta/content/");
define("CACHE_PATH",		"cache/");

define("GRAPHICS",		APP_URL."graphics/");
define("TPL_PATH",		APP_PATH."templates/");
define("TPL_URL",		APP_URL."templates/");
define("VIEW_PATH",		TPL_PATH."content/view/");
define("EDIT_PATH",		TPL_PATH."content/edit/");
?>
