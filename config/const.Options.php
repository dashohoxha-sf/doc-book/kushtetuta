<?php
/**
 * The constants defined in this file change the behaviour of the 
 * framework and the application. You can change the values of the 
 * constants according to the instructions given in comments, or add 
 * new constants that you use in your application.
 */

/**
 * This is the first page of the application. The framework looks
 * for it at the template folder (specified by TPL_PATH). 
 */
define("FIRSTPAGE", "main.html");

/**
 * if this constant is true, the framework will load the DB component
 * and will open a default connection with the db specified in the
 * file 'config/const.DB.php'
 */
define("USES_DB", false);

/**
 * this constant defines the type of DB that the application uses
 * it can be: "MySQL", "Oracle", "ODBC", "ProgreSQL", etc.
 * (except "MySQL", the others are not implemented yet)
 */
define("DB_TYPE", "MySQL");

/**
 * This constant is the value returned by the framework 
 * for a DB variable that has a NULL value. It can be
 * "", "NULL", NULL, etc.
 */
define("NULL_VALUE", "NULL");

/**
 * This constant sets the format of the error message that is displayed
 * when a {{variable}} is not found. 'var_name' is replaced by
 * the actual variable name. Examples: "'var_name' is not defined",
 * "", "undefined", etc. It cannot contain "{{var_name}}" inside.
 */
define("VAR_NOT_FOUND", "{var_name}");

/**
 * When this constant is true, then the CGI vars are displayed
 * at the URL window of the browser. See also SHOW_EXTERNAL_LINK
 * at const.Debug.php.
 */
define("DISPLAY_CGI_VARS", false);

/**
 * When this constant is true or undefined, then the folder
 * names cannot be changed, added or deleted.
 */
//define("STRUCTURE_FIXED", false);

/**
 * When this constant is false, the application will not use
 * any cached pages, but will regenerate them from the scratch.
 * Usually this must be 'true', and only during development
 * should be 'false' (so that modifications are reflected on the page).
 */
define("USE_CACHE", false);

//etc.
?>