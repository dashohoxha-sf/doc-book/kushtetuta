1. Le tasse, le imposte regionali e nazionali, lo sgravio
o l'esonero da esse di alcune categorie di contribuenti,
nonch� le modalit� della loro esazione sono determinate 
dalla legge. In questi casi la legge non pu� avere efficacia
retroattiva.