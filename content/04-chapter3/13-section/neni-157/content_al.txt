1. Sistemi buxhetor p�rb�het nga buxheti i shtetit dhe nga
buxhetet vendore. 

2. Buxheti i shtetit krijohet nga t� ardhurat e mbledhura
prej taksave,tatimeve dhe detyrimeve t� tjera financiare,
si dhe nga t� ardhura t� tjera t� ligjshme. Ai p�rfshin t�
gjitha shpenzimet e shtetit.

3. Organet vendore caktojn� dhe mbledhin taksa dhe detyrime 
t� tjera si� p�rcaktohet me ligj.

4. Organet shtet�rore dhe ato vendore jan� t� detyruara 
t'i b�jn� publike t� ardhurat dhe shpenzimet.
