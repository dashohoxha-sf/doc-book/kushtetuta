1. Gjykata Kushtetuese shqyrton paraprakisht 
kushtetutshm�rin� e ��shjeve t� shtruara p�r referendum 
sipas nenit 150 paragrafet 1 e 2, nenit 151 paragraf�t 
2 e 3, edhe sipas nenit 177 paragraf�t 4 e 5 brenda
60 dit�ve.

2. R�nd�sia e ��shtjeve t� ve�anta, t� parashikuara n� nenin
150 paragrafet	1 e 2 nuk �sht� objekt giykimi n� Gjykat�n 
Kushtetuese.

3. Data e referendumit caktohet nga Presidenti i Republik�s
brenda 45 dit�ve pas shpalljes s� vendimit pozitiv t� 
Gjykat�s Kushtetuese ose pas kalimit t� afatit brenda t�
cilit Gjykata Kushtetuese duhet t� shprehej. Gjat� vitit
referendumet zhvillohen vet�m n� nj� dit�.
