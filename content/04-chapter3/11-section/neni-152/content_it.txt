1. La Corte costituzionale esamina preliminarmente la 
costituzionalit� delle questioni sottoposte al referendum,
secondo l'art. 150 commi 1 e 2, l'art. 151 commi 2 e 3 e 
secondo l'art. 177 commi 4 e 5, entro sessanta giorni.

2. L'importanza di questioni particolari, previste 
all'art. 150 commi 1 e 2,non � sottoposta all'esame della
Corte costituzionale.

3. La data del referendum � fissata dal Presidente della 
Repubblica entro quarantacinque giorni dalla adozione della
decisione positiva della Corte costituzionale o dalla
scadenza del termine entro il quale la Corte costituzionale 
doveva pronunciarsi. Nel corso di ciascun anno tutti i
referendum si svolgono nel medesimo giorno.
