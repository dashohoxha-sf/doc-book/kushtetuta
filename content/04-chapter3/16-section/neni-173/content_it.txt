1. Nel caso di pericolo per il sistema costituzionale e per
la sicurezza pubblica, su richiesta del Consiglio dei 
ministri, si pu� applicare, su parte o su tutto il 
territorio dello Stato, lo stato di emergenza, il quale 
dura fino a quando continui il pericolo, ma comunque non
oltre sessanta giorni.

2. Con l'applicazione dello stato di emergenza l'intervento
delle Forze Armate � consentito previa decisione 
dell'Assemblea e solo quando le forze di polizia non siano
in grado di ristabilire l'ordine.

3. La proroga dello stato di emergenza trova applicazione
solo con il consenso dell'Assemblea da rinnovare ogni 
trenta giorni, per un periodo non superiore a novanta
giorni.
