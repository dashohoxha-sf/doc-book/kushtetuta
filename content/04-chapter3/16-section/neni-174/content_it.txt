1. Per prevenire o evitare le conseguenze di una calamit�
naturale o di un incidente tecnologico, il Consiglio dei 
Ministri pu� decidere, per un periodo non superiore a 
trenta giorni, lo stato di calamit� naturale su una parte 
o su tutto il territorio dello Stato.

2. La proroga dello stato della calamit� naturale � ammessa
solo previo consenso dell' Assemblea.
