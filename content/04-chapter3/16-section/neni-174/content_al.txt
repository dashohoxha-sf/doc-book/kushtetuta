1. P�r parandalimin ose m�njanimin e pasojave t� nj� 
fatkeq�sie natyrore ose aksidenti teknologjik, K�shilli 
i Ministrave mund t� vendos�, p�r nj� periudh� jo m� t� 
gjat� se 30 dit�, gjendjen e fatkeq�sis� natyrore ne nje 
pjes� ose n� t� gjith� territorin e shtetit.

2. Zgjatja e gjendjes s� fatkeq�sis� natyrore mund t� b�het
vet�m me p�lqimin e Kuvendit.
