1. Agli enti locali possono essere delegate, in conformit� 
alla legge, competenze dell' amministrazione statale. Le
spese conseguenti alla delega sono a carico dello Stato.

2. Agli organi delle autorit� locali sono attribuiti 
tributi in conformit� alla legge o secondo accordi. Le spese
concernenti gli obblighi inderogabili degli organi delle 
autorit� locali sono a carico del bilancio dello Stato.
