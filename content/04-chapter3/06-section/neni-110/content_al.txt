1. Qarku p�rb�het nga disa nj�si baz� t� qeverisjes vendore
me lidhje tradicionale, ekonomike e sociale dhe t� 
interesave t� p�rbashk�t.

2. Qarku �sht� nj�sia ku nd�rtohen e zbatohen politikat 
rajonale dhe ku ato harmonizohen me politik�n shtet�rore.

3. Organi p�rfaq�sues i qarkut �sht� k�shilli i qarkut.
Bashkit� dhe komunat delegojn� an�tar� n� k�shillin e qarkut
n� p�rpjes�tim me popullsin� e tyre, por p�r �do rast t� 
pakt�n nj� an�tar. Kryetar�t e komunave dhe t� bashkive jan�
kurdoher� an�tar� t� k�shillit t� qarkut. An�tar�t e tjer� 
t� tij zgjidhen me lista p�rpjes�timore nd�r k�shilltar�t 
bashkiak� ose komunal� nga k�shillat p�rkat�s.

4. K�shilli i qarkut ka t� drejt� t� nxjerr� urdh�resa dhe 
vendime me fuqi	detyruese t� p�rgjithshme p�r qarkun
