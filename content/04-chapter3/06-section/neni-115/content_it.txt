1. L'organo degli enti locali eletto direttamente pu� essere
sciolto o revocato dal Consiglio dei Ministri per gravi 
violazioni della Costituzione o delle leggi.

2. L'organo sciolto o revocato pu� proporre ricorso alla
Corte costituzionale entro quindici giorni; in tal caso la 
decisione del Consiglio dei Ministri � sospesa.

3. In caso di mancato ricorso entro quindici giorni o quando
la Corte costituzionale confermi la decisione del Consiglio 
dei Ministri, il Presidente della Repubblica stabilisce la 
data delle nuove elezioni dell' organo dell'ente locale.
