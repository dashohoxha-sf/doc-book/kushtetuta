1. La candidatura a Presidente viene proposta all'Assemblea 
da un gruppo non inferiore a venti deputati. Ogni deputato 
non pu� partecipare a piu di uno di tali gruppi.

2. L'elezione del Presidente della Repubblica ha luogo a
scrutinio segreto,senza dibattito, a maggioranza dei tre 
quinti dell' Assemblea.

3. Se nella prima votazione non si raggiunge la maggioranza
richiesta,entro sette giorni si effettua una seconda 
votazione.

4. Ove, anche nella seconda votazione, non si raggiunga la 
maggioranza richiesta, si procede ad una terza votazione 
entro sette giorni.

5. Quando i candidati sono pi� di due e nessuno dei due ha
conseguito la maggioranza richiesta, entro sette giorni,
si effettua una quarta votazione tra i due candidati che
hanno ottenuto il numero maggiore di voti.

6. Se alla quarta votazione nessuno dei due candidati ha
conseguito la maggioranza richiesta, si procede ad una 
quinta votazione.

7. Se anche dopo la quinta votazione nessuno dei due 
candidati raggiunge la maggioranza richiesta, l'Assemblea
si scioglie ed entro sessanta giorni si svolgono nuove
elezioni generali.

8. La nuova Assemblea elegge il nuovo Presidente secondo la 
procedura prevista dai precedenti commi del presente 
articolo. Qualora neanche la nuova l'Assemblea elegga il
Presidente, essa si scioglie ed entro 60 giorni si svolgono 
nuove elezioni generali.

9. L'Assemblea successiva elegge il nuovo Presidente della
Repubblica a maggioranza dei suoi componenti.
