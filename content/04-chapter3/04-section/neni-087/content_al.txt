1. Kandidati p�r Presidenti propozohet Kuvendit nga nj� grup
prej jo m� pak se 20 deputet�sh. Nj� deputet nuk lejohet t� 
marr� pjes� n� m� shum� se nj� grup propozues.

2. Presidenti i Republik�s zgjidhet me votim t� fsheht� dhe 
pa debat nga Kuvendi me nj� shumic� prej tri t� pestave t� 
t� gjith� an�tar�ve t� tij.

3. Kur n� votimin e par� nuk arrihet kjo shumic�, 
brenda 7 dit�ve nga kryerja e votimit t� par� b�het nj� 
votim i dyt�. 

4. N� rast se edhe n� votimin e dyt� nuk arrihet kjo 
shumic�, brenda 7 dit�ve b�het nj� votim i tret�.

5. Kur ka m� shum� se dy kandidat� dhe asnj�ri prej tyre nuk
ka marr� shumic�n e k�rkuar, brenda 7 dit�ve b�het nj� votim
i kat�rt nd�rmjet dy kandidat�ve q� kan� marr� numrin m� t�
madh t� votave.

6. N� rast se edhe n� votimin e kat�rt asnj�ri prej dy 
kandidat�ve nuk ka marr� shumic�n e k�rkuar, b�het nj� votim
i pest�.

7. N� rast se edhe pas votimit t� pest� asnj�ri prej dy 
kandidat�ve nuk ka marr� shumic�n e k�rkuar, Kuvendi
shp�rndahet dhe brenda 60 dit�ve b�hen zgjedhjet e reja t� 
p�rgjithshme.

8. Kuvendi i ri zgjedh Presidentin sipas procedur�s s� 
parashikuar nga paragraf�t 1 deri n� 7 t� k�tij neni. 
N� rast se edhe Kuvendi i ri nuk e zgjedh Presidentin, 
ai shp�rndahet dhe brenda 60 dit�ve b�hen zgjedhje t� reja
t� p�rgjithshme.

9. Kuvendi pasardh�s zgjedh Presidentin e Republik�s me 
shumic�n e t� gjith� an�tar�ve t� tij.
