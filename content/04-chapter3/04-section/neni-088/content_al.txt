1. Presidenti i Republik�s, n� �do rast, zgjidhet p�r 
5 vjet, me t� drejt� rizgjedhjeje vet�m nj� her�.

2. Procedura p�r zgjedhjen e Presidentit fillon jo m� von�
se 30 dit� para	mbarimit t� mandatit presidencial paraardh�s.
	
3. Presidenti fillon n� detyr� pasi b�n betimin para 
Kuvendit, por jo m� par� se t� ket� mbaruar mandati i 
Presidentit q� largohet. Presidenti b�n	k�t� betim:
"Betohem se do t'i bindem Kushtetut�s dhe ligjeve t� 
vendit, do t� respektoj t� drejtat dhe lirit� e shtetasve,
do t� mbroj pavar�sin� e Republik�s s� Shqip�ris� dhe do t'i
sh�rbej interesit t� p�rgjithsh�m dhe p�rparimit t� Popullit
Shqiptar." Presidenti mund t� shtoj� edhe: 
"zoti m� ndihmoft�!"

4. Presidenti q� jep dor�heqjen para mbarimit t� mandatit
t� vet, nuk mund t� kandidoj� n� zgjedhjen presidenciale q�
b�het pas dor�heqjes s� tij.
