1. U shtarak�t e sh�rbimit aktiv nuk mund t� zgjidhen ose 
t� em�rohen n� detyra t� tjera shtet�rore dhe as t� marrin
pjes� n� parti ose n� veprimtari politike.

2. Pjes�tar�t e forcave t� armatosura ose personat q� 
kryejn� sh�rbim alternativ, g�zojn� t� gjitha t� drejtat
dhe lirit� kushtetuese, p�rve� rasteve kur ligji parashikon
ndryshe.
