1. I cittadini albanesi hanno il dovere di partecipare alla
difesa della Repubblica dell'Albania, secondo le modalit�
previste dalla legge.

2. Il cittadino che, per motivi di coscienza, non accetti 
il servizio militare nelle forze armate � obbligato a 
prestare un servizio alternativo, secondo le modalit�
previste dalla legge.
