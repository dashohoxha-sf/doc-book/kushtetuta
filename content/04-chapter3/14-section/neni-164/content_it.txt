1. La Corte dei conti presenta all'Assemblea:

a) la relazione per l'applicazione del bilancio dello Stato;

b) il parere sul rapporto del Consiglio dei Ministri per le
spese dell' anno finanziario precedente prima 
dell ' approvazione da parte dell'Assemblea;

c) un'informativa sui risultati dei controlli, quando sia 
richiesta dell' Assemblea.

2. La Corte dei conti presenta all'Assemblea il rapporto
annuale sulla propria attivit�.
