1. La Repubblica albanese non stabilisce una religione ufficiale.

2. Lo Stato � neutrale sulle questioni religiose e di coscienza e 
   garantisce a tutti il diritto di manifestare liberamente il 
   proprio pensiero con la parola nella vita pubblica.
   
3. Lo Stato riconosce l'uguaglianza tra le Comunit� religiose.

4. Lo Stato e le Comunit� religiose rispettano reciprocamente la 
   loro indipendenza e collaborano per il bene di ognuno e di tutti.

5. I rapporti tra lo Stato e le Comunit� religiose si regolano secondo
   gli accordi stipulati tra i loro rappresentanti e il Consiglio dei Ministri.
   Questi accordi vanno ratificati dall'Assemblea.
   
6. Le Comunit� religiose sono persone giuridiche. Esse hanno autonomia di
   gestione dei loro patrimoni secondo i principi, le regole e i canoni 
   a loro propri, purch� non violino gli interessi dei terzi.
