1. Le forze armate garantiscono l'indipendenza del Paese, ne proteggono
   anche l'integrit� territoriale e l'assetto costituzionale.
   
2. Le forze armate sono neutrali rispetto alle questioni politiche e sono
   soggette al controllo civile.
   
3. Nessuna forza militare straniera pu� essere accettata nel territorio 
   albanese, salvo che per legge approvata dalla maggioranza dei componenti
   dell' Assemblea.
