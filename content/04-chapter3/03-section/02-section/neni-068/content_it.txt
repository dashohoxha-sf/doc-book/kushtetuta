1. I candidati a deputato possono essere indicati solo dai
   partiti politici,dalle coalizioni di partiti e dagli 
   elettori.
   
2. Le regole per la designazione dei candidati a deputato,
   per l' organizzazione e lo svolgimento delle elezioni, 
   nonch� la ripartizione dei seggi elettorali e le 
   condizioni di validit� delle elezioni, sono stabilite 
   dalla legge elettorale.
