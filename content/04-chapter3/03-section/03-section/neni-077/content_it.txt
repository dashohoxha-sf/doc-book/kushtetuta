1. L'Assemblea sceglie nel suo seno le commissioni
permanenti e pu� formare commissioni speciali.

2. L'Assemblea ha diritto, e su richiesta di un quarto
dei suoi membri � obbligata, ad istituire una commissione
d'inchiesta per esaminare una questione particolare. 
Le loro conclusioni non sono vincolanti per i tribunali, 
per� se ne pu� dare notizia alla procuratoria, che le valuta
secondo le procedure di legge.

3. Le commissioni d'inchiesta agiscono in conformit� alle 
procedure previste dalla legge.
