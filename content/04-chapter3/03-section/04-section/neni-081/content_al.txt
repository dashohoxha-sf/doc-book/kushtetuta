1. T� drejt�n p�r t� propozuar ligje e ka K�shilli i
Ministrave, �do deputet,si dhe 20 mij� zgjedh�s.

2. Miratohen me tri t� pestat e t� giith� an�tar�ve
t� Kuvendit:

a) ligjet p�r organizimin dhe funksionimin e institucioneve,
t� parashikuara nga Kushtetuta;

b) ligji p�r shtet�sin�;

c) ligji p�r zgjedhjet e p�rgjithshme dhe vendore;

�) ligji p�r referendumet;

d) kodet;

dh) ligji p�r gjendjen e jasht�zakonshme;

e) ligji p�r statusin e funksionar�ve publik�;

�) ligji p�r amnistin�;

f) ligji p�r ndarjen administrative t� Republik�s.
