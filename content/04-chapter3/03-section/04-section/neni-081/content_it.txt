1. Il diritto di proporre leggi appartiene al Consiglio
dei Ministri, al singolo deputato e a ventimila elettori.

2. Vengono approvate a maggioranza dei tre quinti dei membri
dell' Assemblea:

a) le leggi per l'organizzazione e il funzionamento delle 
istituzioni previste dalla Costituzione;

b) la legge sulle cittadinanza;

c) la legge sulle elezioni generali e locali;

d) la legge sui referendum;

e) i codici;

f) la legge sullo stato di emergenza;

g) la legge sullo status dei funzionari pubblici;

h) la legge d'amnistia;

i) la legge di organizzazione amministrativa territoriale
della Repubblica.
