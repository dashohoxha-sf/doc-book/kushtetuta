1. Propozimi i ligjeve, kur �sht� rasti, duhet t� shoq�rohet
kurdoher� me raportin q� p�rligj shpenzimet financiare p�r
zbatimin e tij.

2. Asnj� projektligj joqeveritar q� ben t� nevojshme rritjen
e shpenzimeve t� buxhetit t� shtetit ose q� pak�son t� 
ardhurat, nuk mund t� miratohet pa marr� mendimin e 
K�shillit t� Ministrave, i cili duhet t� shprehet brenda 30
dit�ve nga data e marrjes s� projektligjit.

3. N� qoft� se K�shilli i Ministrave nuk shprehet brenda
afatit t� m�sip�rm,projektligji kalon p�r shqyrtim sipas 
procedur�s s� zakonshme.
