1. Il Parlamento viene eletto per quattro anni.

2. Le elezioni del Parlamento hanno luogo da sessanta a 
   trenta giorni prima del termine del suo mandato e non
   pi� tardi di quarantacinque giorni dopo il suo 
   scioglimento.	,

3. Il mandato dell' Assemblea prosegue sino alla prima 
   riunione del nuovo Parlamento. Nel periodo di prorogatio,
   il Parlamento non pu� emanare leggi o prendere decisioni,
   esclusi i casi di emergenza.
