1. La funzione giurisdizionale � esercitata dalla Corte 
suprema, dalla Corte d'appello e dal Tribunale di primo 
grado, i quali sono istituiti secondo le norme di legge.

2. L'Assemblea pu� istituire con legge Corti aventi
competenze in materie specifiche, ma in nessun caso Corti
straordinarie.
