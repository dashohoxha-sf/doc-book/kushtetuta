1. Il giudice della Corte suprema pu� essere dichiarato
decaduto dall' Assemblea con voto favorevole dei dei due
terzi dei componenti, per violazione della Costituzione,
per aver commesso un reato, per incapacit�fisica e mentale
nonch� per aver compiuto gesti e comportamenti disonorevoli
per la posizione e l'immagine del giudice. La decisione
dell' Assemblea viene riesaminata dalla Corte 
costituzionale, la quale verificando tale motivo, dichiara
la decadenza dalla carica.