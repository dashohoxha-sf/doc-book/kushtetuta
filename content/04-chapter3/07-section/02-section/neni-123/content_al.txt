1. Republika e Shqip�ris�, n� baz� t� marr�veshjeve 
nd�rkomb�tare, u delegon organizatave nd�rkomb�tare 
kompetenca shtet�rore p�r ��shtje t� caktuara.

2. Ligji me t� cilin ratifikohet nj� marr�veshje
nd�rkomb�tare, si� parashikohet n� paragrafin 1 t� k�tij 
neni, miratohet me shumic�n e t� gjith� an�tar�ve t� 
Kuvendit.

3. Kuvendi mund t� vendos� q� ratifikimi i nj� marr�veshjeje
t� till� t� b�het me referendum.
