1. La Repubblica albanese, in base agli accordi
internazionali, pu� delegare alle organizzazioni 
internazionali competenze statali per questioni determinate.

2. La legge, con la quale si ratifica l'accordo 
internazionale, di cui al primo	comma, viene approvata a
maggioranza assoluta.

3. L'Assemblea pu� disporre che tale accordo venga 
ratificato tramite referendum.
