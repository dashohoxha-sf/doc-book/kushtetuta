1. Ligjet, aktet normative t� K�shillit t� Ministrave, t�
ministrave, t� institucioneve t� tjera q�ndrore marrin fuqi
juridike vet�m pasi botohen n� Fletoren Zyrtare.

2. Shpallja dhe botimi i akteve t� tjera normative b�het
sipas m�nyr�s s� parashikuar me ligj.

3. Marr�veshjet nd�rkomb�tare q� ratifikohen me ligj, 
shpallen dhe botohen sipas procedurave q� parashikohen p�r
ligjet. Shpallja dhe botimi i marr�veshjeve t� tjera
nd�rkomb�tare b�het sipas ligjit.
