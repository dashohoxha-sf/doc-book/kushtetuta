1. I regolamenti del Consiglio dei Ministri, dei Ministri e
di altre istituzioni centrali dello Stato, nonch� i decreti 
del Primo Ministro, dei Ministri e dei dirigenti delle
istituzioni centrali hanno carattere interno e sono 
obbligatori solo per le amministrazioni da essi dipendenti.

2. Questi atti sono emanati in conformit� alla legge e non 
possono servire come base per prendere decisioni che 
riguardino il cittadino, le persone giuridiche e gli altri 
soggetti.

3. I regolamenti e le ordinanze si adottano e si applicano
sulla base degli atti che hanno forza giuridica generale.
