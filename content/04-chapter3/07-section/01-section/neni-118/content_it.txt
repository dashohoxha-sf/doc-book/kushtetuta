l. Gli atti subordinati alla legge sono emanati, secondo le
leggi, dagli organi previsti nella Costituzione.

2. La legge deve autorizzare l'emanazione degli atti ad
essa subordinati, determinare l'organo competente, le 
materie che essi possono disciplinare, nonch� i principi 
in base ai quali devono essere adottati.

3. L'organo autorizzato ad emanare atti subordinati alla
legge, ai sensi del secondo comma, non pu� delegare tale 
competenza ad altro organo.
