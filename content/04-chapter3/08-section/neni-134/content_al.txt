1. Gjykata Kushtetuese vihet n� l�vizje vet�m me k�rkes� t�:

a) Presidentit t� Republik�s;

b) Kryeministrit;

c) jo m� pak se nj� s� pest�s s� deputet�ve;

�) Kryetarit t� Kontrollit t� Lart� t� Shtetit;

d) �do gjykate sipas rastit t� nenit 145 pika 2 t� k�saj
Kushtetute;

dh) Avokatit t� Popullit;

e) organeve t� qeverisjes vendore;

�) organeve t� bashk�sive fetare;

f) partive politike dhe organizatave t� tjera;

g) individ�ve.

2. Subjektet e parashikuara nga n�nparagraf�t dh, e, �, f 
dhe g t� paragrafit 1 t� k�tij neni mund t� b�j n� k�rkes� 
vet�m p�r ��shtje q� lidhen me interesat e tyre.
