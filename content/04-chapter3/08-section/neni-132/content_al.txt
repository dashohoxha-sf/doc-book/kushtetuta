1. Vendimet e Gjykat�s Kushtetuese kan� fuqi detyruese t�
p�rgjithshme dhe jan� p�rfundimtare. Gjykata Kushtetuese ka
vet�m t� drejt�n e shfuqizimit t� akteve q� shqyrton.

2. Vendimet e Gjykat�s Kushtetuese hyjn� n� fuqi dit�n e
botimit n� Fletoren Zyrtare. Gjykata Kushtetuese mund t� 
vendos� q� ligji ose akti tjet�r normativ t� shfuqizohet n� 
nj� dat� tjet�r. Mendimi i pakic�s botohet bashk� me
vendimin.
