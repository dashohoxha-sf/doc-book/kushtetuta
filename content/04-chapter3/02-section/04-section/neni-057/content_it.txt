1. Tutti hanno diritto all'istruzione.

2. L'istruzione scolastica obbligatoria � stabilita dalla 
   legge.
   
3. L'istruzione superiore generale pubblica � aperta a tutti.

4. L'istruzione superiore professionale nonch� quella di 
   alto livello viene condizionata soltanto dal criterio 
   della capacit�.
   
5. L'istruzione obbligatoria e quella superiore nelle scuole
   pubbliche sono gratuite.
   
6. Gli alunni e gli studenti si possono istruire anche nelle
   scuole non pubbliche	di tutti i livelli, le quali vengono
   istituite e funzionano a norma di legge.
   
7 L'autonomia delle istituzioni di alto livello e la libert�
  accademica sono garantite dalla legge.
