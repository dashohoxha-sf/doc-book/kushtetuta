1. L'avvocato del popolo difende i diritti, le libert� e 
   gli interessi legittimi dell'individuo dalle azioni ed 
   omissioni illecite ed illegittime della pubblica 
   amministrazione.

2. L'avvocato del popolo � indipendente nell'esercizio delle
  sue funzioni;

3. L'avvocato del popolo ha un bilancio separato, che 
   gestisce autonomamente. Egli predispone il bilancio in 
   conformit� alla legge.
