1. Chiunque, da solo o insieme ad altri, pu� rivolgere una 
   richiesta, un appello o un'osservazione, agli organi pubblici,
   i quali sono obbligati a rispondere  entro i termini e le 
   condizioni indicati dalla legge.