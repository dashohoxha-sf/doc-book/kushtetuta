1. Tutti sono eguali davanti alla legge.

2. Nessuno pu� essere discriminato per sesso, razza, religione,
   etnia, lingua, opinioni politiche, religiose o filosofiche, 
   condizioni economiche e sociali, istruzione, oppure per la 
   provenienza patema.
   
3. Nessuno pu� essere discriminato per i motivi di cui al secondo
   comma se non esiste un motivo obiettivo legalmente riconosciuto.
