1. Askujt nuk mund t'i hiqet liria p�rve�se n� rastet dhe 
sipas procedurave t� parashikuara me ligj.
   
2. Liria e personit nuk mund t� kufizohet, p�rve�se n� rastet
e m�poshtme:

	a) kur �sht� d�nuar me burgim nga gjykata kompetente;
	
	b) p�r moszbatim t� urdhrave t� ligjsh�m t� gjykat�s
	   ose p�r moszbatim t� ndonj� detyrimi t� caktuar 
	   me ligj;
	
	c) kur ka dyshime t� arsyeshme se ka kryer nj� vep�r
	   penale ose p�r t� parandaluar kryetjen prej tij 
	   t� vepr�s penale ose largimin e tij pas kryerjes 
	   s� saj;	.
	
	�) p�r mbik�qyrjen e t� miturit p�r q�llime edukimi 
	ose p�r  shoq�rimin e tij n� organin kompetent;
	
	d) kur personi �sht� p�rhap�s i nj� s�mundjeje 
	ngjit�se, i paaft� mend�risht dhe i rreziksh�m p�r 
	shoq�rin�;
	
	dh) p�r hyrje t� paligjshme n� kufirin shtet�ror, si
	dhe  n� rastet e d�bimit ose t� ekstradimit.
	    
3. Askujt nuk mund t'i hiqet una vet�m p�r shkak se nuk �sht� 
   n� gjendje t� p�rmbush� nj� detyrim kontraktor.
