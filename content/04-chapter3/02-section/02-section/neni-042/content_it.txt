1. La libert�, la propriet� e i diritti riconosciuti dalla Costituzione
   e dalla legge, non possono essere lesi al di fuori di un processo regolare.
   
2. Ognuno, per difendere i diritti, le libert� e i suoi interessi 
   costituzionali e  legali, o nei casi di accuse mosse contro di lui,
   ha diritto ad un giudizio giusto e pubblico entro un termine ragionevole
   da parte di un tribunale  indipendente e neutro previsto dalla legge
