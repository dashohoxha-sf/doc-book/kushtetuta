1. Liria, prona dhe t� drejtat e njohura me Kushtetut� dhe
   me ligj nuk mund t� c�nohen pa nj� proces t� rregullt 
   ligjor.
   
2. Kushdo, p�r mbrojtjen e t� drejtave t� lirive dhe t� 
   interesave t� tij kushtetues dhe ligjor�, ose n� rastin e
   akuzave t� ngritura kund�r tij, ka t� drejt�n e nj�
   gjykimi t� drejt� dhe publik brenda nj� afati t� 
   arsyesh�m nga nj� gjykat� e pavarur dhe e paanshme e 
   caktuar me ligj.
