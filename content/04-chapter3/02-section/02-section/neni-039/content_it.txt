1. Nessun cittadino albanese pu� essere espulso dal territorio del Stato.

2. L'estradizione � ammessa soltanto quando � prevista dagli accordi 
   internazionali di cui la Repubblica albanese � parte, e solo con 
   provvedimento giudiziario.
   
3. � vietata l'espulsione collettiva degli stranieri.
   L'espulsione degli stranieri � concessa alle condizioni
   determinate dalla legge.
