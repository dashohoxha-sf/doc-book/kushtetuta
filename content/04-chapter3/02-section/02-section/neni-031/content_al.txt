1. Gjat� procesit penal kushdo ka t� drejt�:

        a) t� vihet n� dijeni menj�her� dhe holl�sisht p�r
        akuz�n q� i b�het, p�r t� drejtat e tij, si dhe t'i
        krijohet mund�sia p�r t� njoftuar familjen ose t�
        afermit e tij;

        b) t� ket� koh�n dhe leht�sit� e mjaftueshme p�r t�
        p�rgatitur mbrojtjen e vet;

        c) t� ket� ndihm�n pa pages� t� nj� p�rkthyesi, kur
        nuk flet ose nuk kupton gjuh�n shqipe;

	�) t� mbrohet vet� ose me ndihm�n e nj� mbrojt�si
	ligjor  t� zgjedhur prej  tij; t� komunikoj� lirisht
	dhe privatisht me t�, si dhe t'i sigurohet mbrojtja
	falas, kur nuk ka mjete t� mjaftueshme;

	d) t'u b�je pyetje d�shmitar�ve t� pranish�m dhe t�
	k�rkoj� paraqitjen e d�shmitar�ve, t� ekspert�ve dhe
	t� personave t� tjer�, t� cil�t mund t� sqarojn�
	faktet.