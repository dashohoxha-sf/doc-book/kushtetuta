1. � garantita la libert� di coscienza e di religione.

2. Ognuno � libero di scegliere o di cambiare la religione o
   le convinzioni,nonch� di professarle individualmente e collettivamente,
   in pubblico e nella vita privata, attraverso il culto, l'istruzione e 
   le pratiche dei riti.
   
3. Nessuno pu� essere obbligato o privato delle facolt� di partecipazione 
   ad una comunit� religiosa o alle sue pratiche, nonch� di professare in 
   pubblico le proprie convinzioni o la sua fede
