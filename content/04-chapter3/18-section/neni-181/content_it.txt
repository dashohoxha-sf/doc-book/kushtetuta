1. L'Assemblea, entro due/tre anni dall'entrata in vigore 
della presente Costituzione, emana le leggi per la 
sistemazione delle varie questioni concernenti le 
espropriazioni e le requisizioni effettuate prima 
dell' approvazione della. Costituzione vigente, secondo 
la rafia espressa dai criteri dell'art.41.

2. Le leggi e gli altri atti normativi approvati prima 
dell'entrata in vigore della Costituzione vigente 
concernenti le espropriazioni e le requisizioni, trovano 
applicazione allorquando non contrastino con essa.
