1. Pu� essere Ministro chiunque abbia i requisiti per essere
eletto deputato.
2. Il Ministro non pu� esercitare nessuna attivit� statale
n� dirigere o partecipare a organi di societ� con fini di 
lucro.

3. I membri del Consiglio dei Ministri godono dell'immunit�
prevista per i deputati.
