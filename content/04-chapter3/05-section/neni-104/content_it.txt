1. Nel caso in cui la mozione di fiducia presentata dal
Primo ministro venga respinta, l'Assemblea entro quindici
giorni elegge un altro Primo ministro. In questo caso il
Presidente nomina Primo Ministro l'eletto.

2. Quando l'Assemblea non provveda ad eleggere un nuovo 
Primo Ministro, il Presidente della Repubblica ne decreta 
lo scioglimento.

3. La votazione della mozione di fiducia avviene dopo che
siano trascorsi	tre giorni dalla sua presentazione.
