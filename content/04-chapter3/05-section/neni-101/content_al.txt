K�shilli i Ministrave, n� rast nevoje dhe urgjence, n�n
p�rgjegj�sin� e tij, mund t� rixjerr� akte normative q� 
kan� fuqin� e ligjit, p�r marrjen e masave t� p�rkohshme. 
K�to akte normative i d�rgohen menj�her� Kuvendit, i cili 
mblidhet brenda 5 dit�ve n�se nuk �sht� i mbledhur. K�to 
akte humbasin fuqin� q� nga fillimi, n� qoft� se nuk
miratohen nga Kuvendi brenda 45 dit�ve.