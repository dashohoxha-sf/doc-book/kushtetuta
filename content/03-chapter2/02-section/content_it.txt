Dal novembre del 1998 l'Albania ha una Costituzione che la pone a
pieno diritto tra i Paesi democratici ed accoglie le aspirazioni
piu moderne del popolo albanese.

Essa costituisce il punto di arrivo di una aspirazione che parte
dal momento dell'acquisizione dell'indipendenza da parte dello
Stato Albanese. Infatti subito dopo il riconoscimento della
separazione dall 'Impero Ottomano, avvenuta nel 1912, si pens� di
dare un nuovo assetto costituzionale allo Stato albanese. Da una
commissione di controllo internazionale, formata dalle grandi
potenze europee, fu predisposto uno Statuto organico dell'
Albania. Sotto la spinta dei movimenti costituzionali che
avanzavano in tutta l'Europa, questo Statuto prefigurava
l'Albania come Stato costituzionale, su base parlamentare; ma, a
causa delle forti resistenze esercitate dai "poteri"
precostituiti, il Parlamento si vide privato, in concreto, della
sua posizione centrale nell' assetto costituzionale, rimanendo
attributario di un ruolo molto ridotto.

Le vicende della prima guerra mondiale, con l'invasione
dell'Albania da parte degli eserciti stranieri, interruppero il
processo che si era appena avviato. Solo nel 1922 fu possibile
rilanciare un nuovo progetto di costituzione, che prevedeva la
nascita di una Monarchia parlamentare ispirata ai principi della
divisione dei poteri. Questa nuova Costituzione ebbe breve
durata, perch� gi� il2 marzo del 1925 venne sostituita dallo
Statuto Fondamentale della Repubblica di Albania, incentrato sui
poteri del Capo dello Stato, capo anche del Governo e titolare
dell'iniziativa legislativa. La forma prevista era quella
repubblicana. Poco dopo, nel settembre del 1928, Ahmet Zogu, che
guidava lo Stato dopo la vittoria interna (del 1924) contro le
forze progressiste di Fan Noli, fece proclamare, da un' Assemblea
Costituzionale, la Monarchia e si fece eleggere Re degli
Albanesi, come Zog I.

Da questo Statuto fondamentale � nata e si � sviluppa la
costituzione dello Stato zoghista, che provvide all'approvazione
di un nuovo Statuto fondamentale del Regno albanese (entrato in
vigore il 1 dicembre 1928).

I due Statuti, non molto lunghi, avevano preso a modello le
costituzioni del tempo (belga, francese, svizzera, italiana,
austriaca, bulgara). Lo Statuto zoghista si era basato
soprattutto sulla costituzione belga e solo in parte su quella
italiana e bulgara. In esso, tuttavia, al Re venivano attribuiti
poteri di gran lunga piti ampi di quelli riconosciuti ai Sovrani
dalle costituzioni europee del tempo. In particolare la
concentrazione di poteri nelle mani del Re era vastissima, a
spese del Parlamento e del Governo, fino al punto che
l'iniziativa legislativa spettava al Re e non anche al Governo.
Comunque si distingueva tra Capo dello Stato (il Re) e Capo del
Governo, un Primo Ministro di nomina regale.

Nella costituzione zoghista non era lasciato nessuno spazio al
pluralismo politico: la formazione di partiti e di associazioni
politiche era vietata, potendo costituire persino reato. Veniva
riconosciuta formalmente l'indipendenza del potere giudiziario,
che tuttavia era esercitato da giudici di nomina regale.

La Monarchia si propose con carattere di laicit�: perci�, pur
tutelando ed agevolando la famiglia, con successive leggi
riconobbe il divorzio.