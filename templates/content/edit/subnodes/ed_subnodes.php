<?php
/*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class ed_subnodes extends WebObject
{
  function on_add($event_args)
    {
      if (STRUCTURE_FIXED)  return;

      $subnode = $event_args["new_subnode"];
      $node_id = WebApp::getSVar("content->selected");
      $folder = CONTENT_PATH.$node_id.$subnode;
      if (!mkdir($folder, 0775))
        {
          WebApp::error_msg("Coudn't create folder '$subnode'");
        }
      else
        {
          //update cache
          folder::remove_cache_file();
        }
    }

  function onRender()
    {
      $node_id = WebApp::getSVar("content->selected");
      $subnodes = folder::get_subfolders($node_id);
      $rs = new EditableRS("subnodes");
      for ($i=0; $i < sizeof($subnodes); $i++)
        {
          $subnode = $subnodes[$i];
          $rec = array("name" => $subnode, "id" => $node_id.$subnode."/");
          $rs->addRec($rec);
        }
      global $webPage;
      $webPage->addRecordset($rs);
    }
}
?>