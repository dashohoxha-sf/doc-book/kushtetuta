<?php
/*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include "format/chunk_split.php";
include "format/content_structure.php";
include "format/txt_format_funcs.php";

class ed_text_content extends WebObject
{
  function on_add($event_args)
    {
      $node_id = WebApp::getSVar("content->selected");
      $content_file = folder::get_filename($node_id, "content");
      touch($content_file);
    }

  function on_save($event_args)
    {
      $text = $event_args["text"];
      $text = str_replace("\'", "'", $text);
      $text = str_replace('\"', '"', $text);
      $node_id = WebApp::getSVar("content->selected");
      $content_file = folder::get_filename($node_id, "content");
      $fp = fopen($content_file, "w");
      if (!$fp)
        {
          WebApp::message("Could not open file for writting!");
          return;
        }
      fwrite($fp, $text);
      fclose($fp);

      folder::remove_cache_file();
    }

  function on_reformat($event_args)
    {
      $this->reformat();
      folder::remove_cache_file();
    }

  function on_delete($event_args)
    {
      $node_id = WebApp::getSVar("content->selected");
      $content_file = folder::get_filename($node_id, "content");
      unlink($content_file);
      folder::remove_cache_file();
    }

  function onRender()
    {
      $node_id = WebApp::getSVar("content->selected");
      $content_file = folder::get_filename($node_id, "content");
      if (!file_exists($content_file))
        {
          WebApp::addVar("has_content", "false");
        }
      else
        {
          WebApp::addVar("has_content", "true");
          $content_lines = folder::get_lines($node_id, "content");
          $content = implode("\n", $content_lines);
          WebApp::addVar("content", $content);
        }
    }

  function reformat()
    {
      $node_id = WebApp::getSVar("content->selected");

      //get $is_article
      $article_title = folder::get_article_title($node_id);
      $is_article = ($article_title==UNDEFINED ? false : true);

      //get $node_level
      $arr = explode("/", $node_id);
      $node_level = sizeof($arr) - 1;

      //get formated content
      $content_lines = folder::get_lines($node_id, "content");
      if (sizeof($content_lines)==0)
        {
          $content = "";
        }
      else
        {
          $content = format_content($content_lines, $is_article, $node_level);
        }

      //write the formated content
      $content_file = folder::get_filename($node_id, "content");
      $fp = fopen($content_file, "w");
      if (!$fp)
        {
          WebApp::message("Could not open file for writting!");
          return;
        }
      fwrite($fp, $content);
      fclose($fp);
    }

  /**
   * This reformat is for the files that have all the paragraph in
   * a single line (e.g. after copy-paste from word).
   */
  function reformat_1()
    {
      //get the lines of the content
      $node_id = WebApp::getSVar("content->selected");
      $content_file = folder::get_filename($node_id, "content");
      $lines = folder::get_lines($node_id, "content");

      //reformat the content
      $content = "";
      for ($i=0; $i < sizeof($lines); $i++)
        {
          $line = $lines[$i];
          $content .= word_chunk_split($line, 65, "\n") . "\n\n";
        }

      //write the reformated content
      $fp = fopen($content_file, "w");
      if (!$fp)
        {
          WebApp::message("Could not open file for writting!");
          return;
        }
      fwrite($fp, $content);
      fclose($fp);
    }
}
?>