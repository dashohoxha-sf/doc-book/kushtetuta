// -*-C-*- //tells emacs to use the C mode

function add_text_content()
{
  SendEvent('ed_text_content', 'add');
}

function save_text_content()
{
  var form = document.form_txtcontent;
  var text = form.txtcontent.value;
  text = encode_arg_value(text);
  SendEvent('ed_text_content', 'save', "text="+text);
}

function delete_text_content()
{
  var msg="Are you sure you want to delete the content?";
  if (confirm(msg))
    {
      SendEvent('ed_text_content', 'delete');
    }
}

function reformat_text_content()
{
  SendEvent('ed_text_content', 'reformat');
}
