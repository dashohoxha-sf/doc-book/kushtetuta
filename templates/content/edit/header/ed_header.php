<?php
/*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class ed_header extends WebObject
{
  function on_delete_node($event_args)
    {
      if (STRUCTURE_FIXED)  return;

      $node_id = WebApp::getSVar("content->selected");
      $parent_id = folder::get_parent_id($node_id);
      $folder = CONTENT_PATH.$node_id;
      exec("rm -rf $folder", $arr_result, $ret_value);
      if ($ret_value==0)
        {
          WebApp::setSVar("content->selected", $parent_id);
          WebApp::message("Node deleted successfully!");
        }
      else
        {
          WebApp::message("Couldn't delete node!");
        }

      //clear cache
      $parent_id = folder::get_parent_id($node_id);
      $cache_folder = CACHE_PATH.$parent_id;
      exec("rm -rf $cache_folder", $arr_result, $ret_value);      
    }

  function onRender()
    {
      $langs = WebApp::getObject("langs");
      $language = $langs->get_lng_name();
      WebApp::addVar("language", $language);
    }
}
?>