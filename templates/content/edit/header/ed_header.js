// -*-C-*- //tells emacs to use the C mode

function delete_node()
{
  var msg; 
  msg = "You are going to delete this node and \n"
    + "all its subnodes, all their titles, content,\n"
    + "comments etc., in all the languages!! \n\n"
    + "Are you sure you want to delete this node?";
  if (confirm(msg))
    {
      SendEvent('ed_header', 'delete_node');
    }
}
