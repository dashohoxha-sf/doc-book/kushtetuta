<?php
/*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class ed_nodename extends WebObject
{
  function on_save_name($event_args)
    {
      if (STRUCTURE_FIXED)  return;

      $folder_name = $event_args["folder_name"];
      $node_id = WebApp::getSVar("content->selected");
      $parent_id = folder::get_parent_id($node_id);
      $new_node_id = $parent_id.$folder_name."/";
      $folder = CONTENT_PATH.$node_id;
      $new_folder = CONTENT_PATH.$new_node_id;
      exec("mv $folder $new_folder", $arr_result, $ret_value);
      if ($ret_value==0)
        {
          WebApp::setSVar("content->selected", $new_node_id);
          //clear cache
          $parent_id = folder::get_parent_id($node_id);
          $cache_folder = CACHE_PATH.$parent_id;
          exec("rm -rf $cache_folder", $arr_result, $ret_value);      
        }
      else
        {
          $error = implode("\n", $arr_result);
          WebApp::error_msg($error);
        }
    }

  function on_add_title($event_args)
    {
      if (STRUCTURE_FIXED)  return;

      $node_id = WebApp::getSVar("content->selected");
      $title_file = folder::get_filename($node_id, "title");
      touch($title_file);
    }

  function on_delete_title($event_args)
    {
      if (STRUCTURE_FIXED)  return;

      $node_id = WebApp::getSVar("content->selected");
      $title_file = folder::get_filename($node_id, "title");
      unlink($title_file);
      //update cache
      folder::remove_cache_file();
      folder::remove_parent_cache();
    }

  function on_save_title($event_args)
    {
      $title = $event_args["title"];
      $title = str_replace("\'", "'", $title);
      $title = str_replace('\"', '"', $title);
      $node_id = WebApp::getSVar("content->selected");
      $title_file = folder::get_filename($node_id, "title");
      $fp = fopen($title_file, "w");
      if (!$fp)
        {
          WebApp::message("Could not open file for writting!");
          return;
        }
      fwrite($fp, $title);
      fclose($fp);

      //update cache
      folder::remove_cache_file();
      folder::remove_parent_cache();
    }

  function onRender()
    {
      $node_id = WebApp::getSVar("content->selected");
      $folder_name = folder::get_folder_name($node_id);
      $has_title = "true";
      $node_title = folder::get_node_title($node_id);
      if ($folder_name=="")  $folder_name = "(root)";
      if ($node_title==UNDEFINED)  $has_title = "false";
      WebApp::addVars(array(
                            "folder_name" => $folder_name,
                            "has_title"   => $has_title,
                            "node_title"  => $node_title
                            ));
    }
}
?>