// -*-C-*- //tells emacs to use the C mode

function save_node_name()
{
  var form = document.form_nodename;
  var folder_name = form.folder_name.value;
  SendEvent('ed_nodename', 'save_name', 'folder_name='+folder_name);
}

function add_node_title()
{
  SendEvent('ed_nodename', 'add_title');
}

function save_node_title()
{
  var form = document.form_nodename;
  var node_title = form.node_title.value;
  node_title = encode_arg_value(node_title);
  SendEvent('ed_nodename', 'save_title', 'title='+node_title);
}

function del_node_title()
{
  var msg="Are you sure you want to delete the title of this node?";
  if (confirm(msg))
    {
      SendEvent('ed_nodename', 'delete_title');
    }
}
