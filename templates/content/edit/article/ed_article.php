<?php
/*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class ed_article extends WebObject
{
  function on_add_comments($event_args)
    {
      $node_id = WebApp::getSVar("content->selected");
      $comments_file = folder::get_filename($node_id, "comments");
      touch($comments_file);
    }

  function on_delete_comments($event_args)
    {
      $node_id = WebApp::getSVar("content->selected");
      $comments_file = folder::get_filename($node_id, "comments");
      unlink($comments_file);
      folder::remove_cache_file();
    }

  function on_save_comments($event_args)
    {
      $text = $event_args["text"];
      $text = str_replace("\'", "'", $text);
      $text = str_replace('\"', '"', $text);

      $node_id = WebApp::getSVar("content->selected");
      $comments_file = folder::get_filename($node_id, "comments");
      $fp = fopen($comments_file, "w");
      if (!$fp)
        {
          WebApp::message("Could not open file for writting!");
          return;
        }
      fwrite($fp, $text);
      fclose($fp);

      //remove cache file
      $lng = WebApp::getSVar("langs->selected");
      $cache_file = CACHE_PATH.$node_id."content_".$lng.".html";
      exec("rm $cache_file", $arr_result);
      folder::remove_cache_file();
    }

  function onRender()
    {
      $node_id = WebApp::getSVar("content->selected");
      $comments_file = folder::get_filename($node_id, "comments");
      if (!file_exists($comments_file))
        {
          WebApp::addVar("has_comments", "false");
        }
      else
        {
          WebApp::addVar("has_comments", "true");
          $comments_lines = folder::get_lines($node_id, "comments");
          $comments = implode("\n", $comments_lines);
          WebApp::addVar("comments", $comments);
        }
    }
}
?>