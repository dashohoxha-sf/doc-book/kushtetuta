// -*-C-*- //tells emacs to use the C mode

function add_comments()
{
  SendEvent('ed_article', 'add_comments');
}

function save_comments()
{
  var form = document.form_comments;
  var text = form.comments.value;
  text = encode_arg_value(text);
  SendEvent('ed_article', 'save_comments', "text="+text);
}

function delete_comments()
{
  var msg="Are you sure you want to delete the comments?";
  if (confirm(msg))
    {
      SendEvent('ed_article', 'delete_comments');
    }
}
