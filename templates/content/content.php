<?php
/*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once TPL_PATH."content/class.Node.php";
include_once TPL_PATH."content/class.folder.php";

class content extends WebObject
{
  function init()
    {
      $this->addSVar("selected", "");
      $this->addSVar("mode", "view");  //mode can be 'view' or 'edit'
    }

  function on_select($event_args)
    {
      $node = $event_args["node"];
      $this->setSVar("selected", $node);
    }

  function on_view($event_args)
    {
      $this->setSVar("mode", "view");
    }

  function on_edit($event_args)
    {
      $this->setSVar("mode", "edit");
    }

  function onParse()
    {
      $mode = $this->getSVar("mode");
      $node_id = $this->getSVar("selected");
      $node_type = folder::get_node_type($node_id);
      if ($mode=="edit")
        $file =  "edit/".$node_type."/ed_".$node_type.".html";
      else
        $file = "view/".$node_type."/".$node_type.".html";
      WebApp::addVar("file", $file);
    }

  function onRender()
    {
      WebApp::addVars($this->get_tpl_vars());
    }

  /**
   * Returns an associative array with the template variables
   * next_id, prev_id, up_id, toc_id, next_title, prev_title,
   * up_title, toc_title, node_title
   */
  function get_tpl_vars()
    {
      $toc_id = "";
      $node_id = WebApp::getSVar("content->selected");
      $up_id = folder::get_parent_id($node_id);

      global $folder_prev_node_id, $folder_next_node_id;
      $folder_prev_node_id = '';
      $folder_next_node_id = '';
      $node = new Node("");
      $node->browse("get_prev_next");
      $prev_id = $folder_prev_node_id;
      $next_id = $folder_next_node_id;

      $vars = array(
                    "toc_id"     => $toc_id,
                    "up_id"      => $up_id,
                    "next_id"    => $next_id,
                    "prev_id"    => $prev_id,
                    "node_title" => folder::get_title($node_id),
                    "toc_title"  => folder::get_title($toc_id),
                    "up_title"   => folder::get_title($up_id),
                    "next_title" => folder::get_title($next_id),
                    "prev_title" => folder::get_title($prev_id)
                    );
      return $vars;
    }
}

function get_prev_next(&$node)
{
  static $prev_id = "";

  $has_no_title = (folder::get_node_title($node->id) == UNDEFINED);
  $is_not_an_article = (folder::get_article_title($node->id) == UNDEFINED);
  if ($has_no_title and $is_not_an_article)  return;

  $node_id = WebApp::getSVar("content->selected");
  if ($node->id==$node_id)
    {
      global $folder_prev_node_id;
      $folder_prev_node_id = $prev_id;
    }
  if ($prev_id==$node_id)
    {
      global $folder_next_node_id;
      $folder_next_node_id = $node->id;
    }
  $prev_id = $node->id;
}
?>