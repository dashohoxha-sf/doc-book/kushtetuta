<?php
/*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 *  Class Node represents an item in the TOC.
 */
class Node
{
  /** identifies this toc item */
  var $id;

  /** title of the node */
  var $title;

  /** array of subnodes */
  var $subnodes;

  /** Construct this node */
  function Node($id)
    {
      $this->id = $id;
      $this->title = UNDEFINED;
      $this->subnodes = array();
      $this->construct_from_folder();
    }

  /**
   * Makes a pre-order browsing of the branch starting 
   * with this node and applies the function to each node.
   */
  function browse($fun_name)
    {
      $fun_name($this);
      for ($i=0; $i < sizeof($this->subnodes); $i++)
        {
          $child = $this->subnodes[$i];
          $child->browse($fun_name);
        }
    }

  /** Construct this node taking the id as the root folder */
  function construct_from_folder()
    {
      $folder = $this->id;
      $this->title = folder::get_node_title($folder);
      $subfolders = folder::get_subfolders($folder);

      //create child items from subfolders
      for ($i=0; $i < sizeof($subfolders); $i++)
        {
          $fname = $subfolders[$i];
          $subfolder = $folder.$fname.'/';
          $node = new Node($subfolder);
          $this->subnodes[] = $node;
        }
    }

  /*-------------- output functions -----------------*/

  /** 
   * Returns the index of the branch 
   * starting with this node, in HTML format.
   */
  function get_index()
    {
      $html = "\n  <dl>\n";
      for ($i=0; $i < sizeof($this->subnodes); $i++)
        {
          $node = $this->subnodes[$i];
          if ($node->title==UNDEFINED)  continue;
          $html .= $node->to_html("    ");
        }
      $html .= "  </dl>\n";
      return $html;
    }

  /** 
   * Returns the node and all its subnodes in HTML format.
   */
  function to_html($ident)
    {
      $level = folder::get_node_level($this->id);
      $a_href = '<a href="javascript: content_select(\''.$this->id.'\')">';
      $html = $ident.'<dt class="titull'.$level.'">'."\n"
        . $ident.'  '.$a_href
        . $this->title."</a>\n"
        . $ident."</dt>\n";

      //get an array of subnodes
      $subnodes = array();
      for ($i=0; $i < sizeof($this->subnodes); $i++)
        {
          $child = &$this->subnodes[$i];
          if ($child->title==UNDEFINED)  continue;
          $subnodes[] = $child;
        }

      if (sizeof($subnodes)==0)   return $html;

      $html .= $ident."<dd>\n".$ident."  <dl>\n";
      for ($i=0; $i < sizeof($subnodes); $i++)
        {
          $subnode = $subnodes[$i];
          $html .= $subnode->to_html("    ".$ident);
        }
      $html .= $ident."  </dl>\n".$ident."</dd>\n";

      return $html;
    }

  /** 
   * Returns the node and all its subnodes
   * in an easily readable text format.
   */
  function to_text($ident)    //debug
    {
      $txt_content = $ident . "> " . $this->id . " -> " . $this->title . "\n";
      for ($i=0; $i < sizeof($this->subnodes); $i++)
        {
          $child = $this->subnodes[$i];
          $txt_content .= $child->to_text("    ".$ident);
        }
      return $txt_content;
    }
}
?>
