<?php
/*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * Class folder contains some auxiliary functions.
 */
class folder
{
  function get_title($node_id)
    {
      $title = folder::get_node_title($node_id);
      if ($title==UNDEFINED)
        {
          $title = folder::get_article_title($node_id);
        }
      return $title;
    }

  /**
   * Get the title of the given node, 
   * according to the selected language.
   */
  function get_node_title($node_id)
    {
      $folder_path = CONTENT_PATH.$node_id;
      $lng = WebApp::getSVar("langs->selected");
      $title_file = $folder_path."title_".$lng.".txt";
      if (file_exists($title_file))
        {
          $arr_file = file($title_file);
          $title = trim($arr_file[0]);
        }
      else
        {
          $title = UNDEFINED;
        }

      return $title;
    }

  /** Returns the title of an article */
  function get_article_title($node_id)
    {
      if (!ereg("/neni-", $node_id))  return UNDEFINED;

      $art_nr = substr($node_id, -4, 3) + 0;
      $langs = WebApp::getObject("langs");
      $artic = $langs->get_text("Article");
      return $artic." ".$art_nr;
    }

  /** 
   * Returns the path of the file 'filetype_lng.txt'
   * (e.g. 'content_al.txt'), or UNDEFINED if it does not exists.
   */
  function get_filename($node_id, $filetype)
    {
      $folder_path = CONTENT_PATH.$node_id;
      $lng = WebApp::getSVar("langs->selected");
      $filename = $folder_path.$filetype."_".$lng.".txt";
      return $filename;
    }

  /**
   * Returns the array of the content lines of the 
   * given node, according to the selected language.
   * $filetype is 'content', comments', etc. 
   */
  function get_lines($node_id, $filetype)
    {
      $filename = folder::get_filename($node_id, $filetype);
      if (!file_exists($filename))  return array();

      $arr_lines = file($filename);
      for ($i=0; $i < sizeof($arr_lines); $i++)
        {
          $line = $arr_lines[$i];
          $arr_lines[$i] = chop($line);
        }

      return $arr_lines;
    }

  /**
   * Get the content of the given node, 
   * according to the selected language,
   * converting it somehow to HTML.
   * $filetype is 'content', comments', etc. 
   */
  function get_html($node_id, $filetype)
    {
      $lines = folder::get_lines($node_id, $filetype);
      $content = "<p>\n";
      for ($i=0; $i < sizeof($lines); $i++)
        {
          $line = $lines[$i];
          $content .= (trim($line)=="" ? "</p>\n<p>\n" : $line."\n");
        }
      $content .= "</p>";

      return $content;
    }

  /**
   * Returns the type of the given node.
   * The type can be: 'article', 'article_list', 'node'.
   * 'article_list' is a special node that can contain only
   * articles as subnodes, and 'article' is a leaf node that
   * does not have subnodes.
   */
  function get_node_type($node_id)
    {
      $article_title = folder::get_article_title($node_id);
      if ($article_title<>UNDEFINED)  return "article";

      $subfolders = folder::get_subfolders($node_id);
      if (sizeof($subfolders)==0)  return "node";

      for ($i=0; $i < sizeof($subfolders); $i++)
        {
          $subfolder = $subfolders[$i];
          if (ereg("^neni-...\$", $subfolder))  continue;
          else  //there is a subfolder that is not an article
            return "node";
        }
      //all the subfolders are articles
      return "article_list";
    }

  /**
   * Returns an array of the subfolders of the given folder. 
   */
  function get_subfolders($node_id)
    {
      $subfolders = array();

      //open the folder
      $folder = CONTENT_PATH.$node_id;
      $dir = opendir($folder);
      if (!$dir)
        {
          print "get_subfolders(): Cannot open folder '$folder'."; 
          return $subfolders;
        }

      //get subfolders
      while ($fname = readdir($dir))
        {
          if (filetype($folder.$fname) == 'dir')
            {
              if ($fname=="." or $fname==".." 
                  or $fname=="CVS" or $fname==".svn")  continue;
              $subfolders[] = $fname;
            }
        }
      closedir($dir);
  
      sort($subfolders);
      return $subfolders;
    }

  /** Returns the level of the given node in the TOC */
  function get_node_level($node_id)
    {
      $level = 0;
      while ($node_id<>"")
        {
          $node_id = folder::get_parent_id($node_id);
          $level++;
        }
      return $level;
    }

  /** Returns the id of the parent node. */
  function get_parent_id($node_id)
    {
      $folder = folder::get_folder_name($node_id);
      $parent_id = ereg_replace($folder."/\$", "", $node_id);
      return $parent_id;
    }

  /** Returns the name of the folder */
  function get_folder_name($node_id)
    {
      ereg("([^/]*)/\$", $node_id, $regs);
      $folder = $regs[1];
      return $folder;
    }

  function remove_cache_file()
    {
      $node_id = WebApp::getSVar("content->selected");
      $lng = WebApp::getSVar("langs->selected");
      $cache_file = CACHE_PATH.$node_id."content_".$lng.".html";
      exec("rm $cache_file", $arr_result);
    }

  /** remove the cache of the parent node as well */
  function remove_parent_cache()
    {
      $node_id = WebApp::getSVar("content->selected");
      $parent_id = folder::get_parent_id($node_id);
      $lng = WebApp::getSVar("langs->selected");
      $parent_cache_file = CACHE_PATH.$parent_id."content_".$lng.".html";
      exec("rm $parent_cache_file", $arr_result);
    }
}
?>