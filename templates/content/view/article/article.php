<?php
/*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class article extends WebObject
{
  function onRender()
    {
      $node_id = WebApp::getSVar("content->selected");
      $node_comments = folder::get_html($node_id, "comments");
      $file_comments = folder::get_filename($node_id, "comments");
      $has_comments = (file_exists($file_comments) ? 'true' : 'false');
      WebApp::addVars(array(
                            "has_comments"  => $has_comments,
                            "node_comments" => $node_comments
                            ));
      $langs = WebApp::getObject("langs");
      $msg_comments = $langs->get_text("Comments");
      WebApp::addVar("Comments", $msg_comments);
    }
}
?>