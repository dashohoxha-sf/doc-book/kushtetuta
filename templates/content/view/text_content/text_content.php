<?php
/*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include "format/chunk_split.php";
include "format/content_structure.php";
include "format/html_format_funcs.php";

class text_content extends WebObject
{
  function onRender()
    {
      $node_id = WebApp::getSVar("content->selected");
      $node_content = $this->get_html_content($node_id);
      $file_content = folder::get_filename($node_id, "content");
      $has_content = (file_exists($file_content) ? 'true' : 'false');
      WebApp::addVars(array(
                            "has_content" => $has_content,
                            "CONTENT"     => $node_content
                            ));

    }
  
  function get_html_content($node_id)
    {
      //get $is_article
      $article_title = folder::get_article_title($node_id);
      $is_article = ($article_title==UNDEFINED ? false : true);

      //get $node_level
      $arr = explode("/", $node_id);
      $node_level = sizeof($arr) - 1;

      //get formated content
      $content_lines = folder::get_lines($node_id, "content");
      if (sizeof($content_lines)==0)
        {
          $content = "";
        }
      else
        {
          //constant LNG is used by replace_chars()
          //(called by format_content())
          $lng = WebApp::getSVar("langs->selected");
          define("LNG", $lng); 

          $content = format_content($content_lines, $is_article, $node_level);
        }

      return $content;
    }
}
?>