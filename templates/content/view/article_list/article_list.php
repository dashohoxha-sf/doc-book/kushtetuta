<?php
/*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class article_list extends WebObject
{
  function onRender()
    {
      $node_id = WebApp::getSVar("content->selected");
      $node = new Node($node_id);
      $rs = $this->get_article_rs($node);
      global $webPage;
      $webPage->addRecordset($rs);      
    }

  /** Returns a recordset with the articles of this node */
  function get_article_rs($node)
    {
      $rs = new EditableRS("articles");
      for ($i=0; $i < sizeof($node->subnodes); $i++)
        {
          $node_id = $node->subnodes[$i]->id;

          //get the article title
          $article_title = folder::get_article_title($node_id);
          if ($article_title==UNDEFINED)  continue;

          //get the summary
          $content_lines = folder::get_lines($node_id, "content");
          $content = implode(" ", $content_lines);
          $summary = substr($content, 0, 80) . " . . .";

          //create and add the record
          $rec = array(
                       "id"      => $node_id,
                       "artic"   => $article_title,
                       "summary" => $summary
                       );
          $rs->addRec($rec);
        } 

      return $rs;
    }
}
?>