<?php
/*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 *  Class MenuItem represents an item of a menu.
 */
class MenuItem
{
  /** identifies this menu item */
  var $id;

  /** text that will be shown in the menu */
  var $label;

  /** what will be done when this element is selected 
   *  (typically a JS function call) */
  var $action;

  /** array of subitems */
  var $children;

  /** Construct this menu item from the given folder */
  function MenuItem($folder ="")
    {
      $this->id = $folder;
      $this->label = UNDEFINED;
      $this->action = UNDEFINED;
      $this->children = array();
      $this->construct_from_folder();
    }

  function addChild(&$ch)
    {
      $this->children[] = $ch;
      $ch->parent = &$this;
    }

  function nrChildren()
    {
      return sizeof($this->children);
    }

  function hasChildren()
    {
      $nr_ch = $this->nrChildren();
      return ($nr_ch <> 0);
    }

  function construct_from_folder()
    {
      $folder = $this->id;
      $folder_path = CONTENT_PATH.$folder;

      //get the title and set the label and action
      $title_file = $folder_path."title_".LNG.".txt";
      if (file_exists($title_file))
        {
          $arr_file = file($title_file);
          $title = trim($arr_file[0]);
        }
      else
        {
          $title = $this->id;
        }
      $title = str_replace("'", "\\'", $title);
      $this->label = $title;
      $this->action = 'javascript: alert(\''.$this->id.'\')';

      //open the folder
      $dir = opendir($folder_path);
      if (!$dir)
        {
          print "MenuItem->construct_from_folder(): "
            . "Cannot open folder '$folder_path'."; 
          return;
        }
      //create child items from subfolders
      while ($fname = readdir($dir))
        {
          if (filetype($folder_path.$fname) == 'dir')
            {
              if ($fname=="." or $fname==".." or $fname=="CVS")  continue;
              $subfolder = $folder.$fname.'/';
              $item = new MenuItem($subfolder);
              $this->addChild($item);
            }
        }
      closedir($dir);
    }

  /*-------------- output functions -----------------*/

  /** 
   * Returns the element and all its subelements
   * in an easily readable text format.
   */
  function to_text($ident)    //debug
    {
      $txt_menu = $ident."> ".$this->id." -> ".$this->label." -> ".$this->action."\n";
      for ($i=0; $i < $this->nrChildren(); $i++)
        {
          $child = $this->children[$i];
          $txt_menu .= $child->to_text("\t".$ident);
        }
      return $txt_menu;
    }

  /**
   * Returns the whole menu as a JS array of menu items.
   * It must be called only for the root item.
   */
  function to_js()
    {
      $js_menu = "var MENU_ITEMS = [";
      for ($i=0; $i < $this->nrChildren(); $i++)
        {
          $child = $this->children[$i];
          $js_menu .= "\n";
          $js_menu .= $child->item_to_js("    ");
          $js_menu .= ",";
        }
      $js_menu .= "\n];\n";
      return $js_menu;
    }

  /**
   * Returs this item and its children as a JS array.
   */
  function item_to_js($ident)
    {
      $js_array = $ident."['".$this->label."', "
        . "'javascript:toc_select(\'" . $this->id . "\')'";
      if (!$this->hasChildren())
        {
          //close the array
          $js_array .= "]";
        }
      else
        {
          for ($i=0; $i < $this->nrChildren(); $i++)
            {
              $child = $this->children[$i];
              $ch_ident = "    ".$ident;  //increase the identation of the child
              $js_array .= ",\n"
                . $ch_ident . $child->item_to_js($ch_ident);
            }
          //close the array
          $js_array .= "\n".$ident."]";
        }
      return $js_array;
    }
}
?>