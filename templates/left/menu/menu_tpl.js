// -*-C-*- //tells emacs to use the C mode

/* --- geometry and timing of the menu --- */
var MENU_POS = new Array();
    // item sizes for different levels of menu
    MENU_POS['width'] = [128, 168, 168];
    MENU_POS['height'] = [18, 18, 18];
    // menu block offset from the origin:
    //	for root level origin is upper left corner of the page
    //	for other levels origin is upper left corner of parent item
    MENU_POS['block_top'] = [85, 3, 3];
    MENU_POS['block_left'] = [0, 120, 160];
    // offsets between items of the same level
    MENU_POS['top'] = [20, 20, 20];
    MENU_POS['left'] = [0, 0, 0];
    // time in milliseconds before menu is hidden after cursor has gone out
    // of any items
    MENU_POS['hide_delay'] = [100, 100, 100];

/* --- dynamic menu styles ---
   note: you can add as many style properties as you wish but be not all browsers
   are able to render them correctly. The only relatively safe properties are
   'color' and 'background'.
*/
var MENU_STYLES = new Array();
    // default item state when it is visible but doesn't have mouse over
    MENU_STYLES['onmouseout'] = [
				 'color', ['#000044', '#000044', '#000044'],
				 'background', ['#dddddd', '#dddddd', '#dddddd'],
				 'border', ['1px outset #ffffff', 
					    '1px outset #ffffff', 
					    '1px outset #ffffff'],
				 ];
    // state when item has mouse over it
    MENU_STYLES['onmouseover'] = [
				  //'color', ['#000000, '#000000', '#000000'],
				  'background', ['#eeeeee', '#eeeeee', '#eeeeee'],
				  ];
    MENU_STYLES['onmousedown'] = [
				  //'color', ['#000000', '#000000', '#000000'],
				  'background', ['#eef9f9', '#eef9f9', '#eef9f9'],
				  'border', ['1px inset #ffffff', 
					     '1px inset #ffffff', 
					     '1px inset #ffffff'],
				  ];

