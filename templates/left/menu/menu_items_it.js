// -*-C-*- //tells emacs to use the C mode

var MENU_ITEMS = [
    ['I principi fondamentali', 'javascript:content_select(\'04-chapter3/01-section/\')'],
    ['Il Parlamento', 'javascript:content_select(\'04-chapter3/03-section/\')',
        ['1. L\'elezione e la durata', 'javascript:content_select(\'04-chapter3/03-section/01-section/\')'],
        ['2. I deputati', 'javascript:content_select(\'04-chapter3/03-section/02-section/\')'],
        ['3. L\'organizzazione e il funzionamento', 'javascript:content_select(\'04-chapter3/03-section/03-section/\')'],
        ['4. Il procedimento legislativo', 'javascript:content_select(\'04-chapter3/03-section/04-section/\')'],
    ],
    ['Il Presidente ', 'javascript:content_select(\'04-chapter3/04-section/\')',

    ],
    ['La Magistratura', null,
        ['1. I precedenti progetti ', 'javascript:content_select(\'chapter2/01-section/\')'],
        ['2. Le aspirazioni di nuovo assetto costituz', 'javascript:content_select(\'chapter2/02-section/\')'],
        ['3. Rilievi conclusivi', 'javascript:content_select(\'chapter2/01-section0/\')'],
        ['4. Sovranit? eguaglianza ', 'javascript:content_select(\'chapter2/04-section/\')'],
        ['5. Potere legislativo ed esecutivo', 'javascript:content_select(\'chapter2/05-section/\')'],
        ['6. Potere giudiziario e Pubblico Ministero', 'javascript:content_select(\'chapter2/06-section/\')'],
        ['7. La Corte costituzionale', 'javascript:content_select(\'chapter2/07-section/\')'],
        ['8. Autonomie locali', 'javascript:content_select(\'chapter2/08-section/\')'],
        ['9. Il referendum', 'javascript:content_select(\'chapter2/09-section/\')']
    ],
    ['Il Referendum', 'javascript:content_select(\'04-chapter3/01-section1/\')',
        ['04-chapter3/02-section/', 'javascript:content_select(\'04-chapter3/02-section/\')',
                ['04-chapter3/02-section/title1/', 'javascript:content_select(\'04-chapter3/02-section/01-section/\')'],
                ['04-chapter3/02-section/title2/', 'javascript:content_select(\'04-chapter3/02-section/02-section/\')'],

        ],
        ['04-chapter3/03-section/', 'javascript:content_select(\'04-chapter3/03-section/\')',
                ['04-chapter3/03-section/01-section/', 'javascript:content_select(\'04-chapter3/03-section/title1/\')'],
                ['04-chapter3/03-section/02-section/', 'javascript:content_select(\'04-chapter3/03-section/02-section/\')'],

        ],

        ['04-chapter3/09-section/', 'javascript:content_select(\'04-chapter3/09-section/\')']
    ],
];
