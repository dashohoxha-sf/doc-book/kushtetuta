// -*-C-*- //tells emacs to use the C mode

var MENU_ITEMS = [
    ['Parime Themelore', 'javascript:content_select(\'04-chapter3/01-section/\')'],
    ['Parlamenti ', 'javascript:content_select(\'04-chapter3/03-section/\')',
        ['Zgjedhja dhe Afati', 'javascript:content_select(\'04-chapter3/03-section/01-section/\')'],
        ['Deputet�t', 'javascript:content_select(\'04-chapter3/03-section/02-section/\')'],
        ['Organizimi dhe Funksionimi', 'javascript:content_select(\'04-chapter3/03-section/03-section/\')'],
        ['Pro�esi Ligjv�n�s', 'javascript:content_select(\'04-chapter3/03-section/04-section/\')'],
    ],
    ['Gjykatat', 'javascript:content_select(\'04-chapter3/08-section/\')',
        ['Gjykata Kushtetuese', 'javascript:content_select(\'04-chapter3/08-section/\')'],
        ['Gjykatat', 'javascript:content_select(\'04-chapter3/09-section/\')'],
        ['Prokuroria', 'javascript:content_select(\'04-chapter3/10-section0/\')'],
    ],
    ['Organet Drejtuese', null,
        ['Presidenti', 'javascript:content_select(\'04-chapter3/04-section/\')'],
        ['K�shilli i Ministrave', 'javascript:content_select(\'04-chapter3/05-section/\')'],
        ['Qeverisja Vendore', 'javascript:content_select(\'04-chapter3/06-section/\')'],

    ],
    ['T� drejtat e njeriut', 'javascript:content_select(\'04-chapter3/02-section\')',
        ['Parime t� P�rgjithshme', 'javascript:content_select(\'04-chapter3/02-section/01-section/\')'],
        ['Lirit� dhe t� drejtat vetjake', 'javascript:content_select(\'04-chapter3/02-section/02-section/\')'],
        ['Lirit� dhe t� drejtat politike', 'javascript:content_select(\'04-chapter3/02-section/03-section/\')'],
        ['Lirit� dhe t� drejtat ekonomike', 'javascript:content_select(\'04-chapter3/02-section/04-section/\')'],
        ['Objektivat social�', 'javascript:content_select(\'04-chapter3/02-section/05-section/\')'],
        ['04-chapter3/02-section/', 'javascript:content_select(\'04-chapter3/02-section/\')',
            ['04-chapter3/02-section/title1/', 'javascript:content_select(\'04-chapter3/02-section/01-section/\')'],

        ],


    ],
];
