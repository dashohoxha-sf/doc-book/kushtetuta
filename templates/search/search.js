// -*-C-*- //tells emacs to use the C mode

function new_search()
{
  var form = document.form_new_search;
  var words = form.new_search_words.value;
  SendEvent('search', 'new_search', 'words='+words);
}

function close_search()
{
  SendEvent('contentbox', 'display_content');
}

function search_help()
{
  var msg = "Instructions about how to use searching!";
  alert(msg);
}

function display_content(node_id)
{
  SendEvent('contentbox', 'display_content', 'node_id='+node_id);
}

function content_select(node_id)
{
  //content_select() is used by the left menu,
  //but since the content is hidden, this function is not declared;
  //we declare it here, so that the menu is still fuctional
  display_content(node_id);
}
