<?php
/*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once TPL_PATH."content/class.folder.php";

class search extends WebObject
{
  function init()
    {
      $this->addSVar("words", "");
    }

  function on_new_search($event_args)
    {
      $this->setSVar("words", $event_args["words"]);
    }

  function onRender()
    {
      $rs = $this->get_search_results_rs();
      global $webPage;
      $webPage->addRecordset($rs);
    }

  /** Returns a recordset of search results. */
  function get_search_results_rs()
    {
      $rs = new EditableRS("search_results");

      //construct and execute the search command
      $swish_search = APP_PATH."search/swish-search";
      $words = $this->getSVar("words");
      if (trim($words)=='')  return $rs;
      $lng = WebApp::getSVar("langs->selected");
      $index_file = APP_PATH."search/index.".$lng;
      $cmd_search = $swish_search." -H0 -d:: -w '$words' -f ".$index_file;
      $search_output = `$cmd_search`;

      //fill the recordset from the search output
      $lines = explode("\n", $search_output);
      for ($i=0; $i < sizeof($lines)-1; $i++)
        /*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
        */

        {
          list($rank,$file,$title,$length) = explode("::", $lines[$i]);
          $file = str_replace(CONTENT_PATH, '', $file);
          $node_id = str_replace($title, '', $file);
          $node_title = folder::get_title($node_id);
          $rec = array(
                       "nr" => $i+1,
                       "node_id" => $node_id,
                       "node_title" => $node_title
                       );
          $rs->addRec($rec);
        }
      return $rs;
    }
}
?>