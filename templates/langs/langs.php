<?php
/*
This file is part of kushtetuta.  kushtetuta is a web application that
presents online the Constitution of the Republic of Albania.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

kushtetuta is free software; you  can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

kushtetuta  is distributed in  the hope  that it  will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with kushtetuta; if not,  write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class langs extends WebObject
{
  /** an array that keeps the language specific messages */
  var $messages = array();

  function init()
    {
      $this->addSVar("selected", "al");
    }

  function on_select($event_args)
    {
      $lng = $event_args["lng"];
      $this->setSVar("selected", $lng);
    }

  function onParse()
    {
      $lng = $this->getSVar("selected");
      $lng_file = TPL_PATH."langs/text_".$lng.".php";
      if (file_exists($lng_file))  include_once $lng_file;
      $this->messages = $messages;
    }

  function onRender()
    {
      $lng = $this->getSVar("selected");
      $rs = new EditableRS("langs");
      $rec = array(
                   "lng"   => "al",
                   "class" => ($lng=="al" ? "lang-selected" : "lang"),
                   "label" => "AL"
                   );
      $rs->addRec($rec);
      $rec = array(
                   "lng"   => "it",
                   "class" => ($lng=="it" ? "lang-selected" : "lang"),
                   "label" => "IT"
                   );
      $rs->addRec($rec);     
      $rec = array(
                   "lng"   => "fr",
                   "class" => ($lng=="fr" ? "lang-selected" : "lang"),
                   "label" => "FR"
                   );
      $rs->addRec($rec);
      $rec = array(
                   "lng"   => "en",
                   "class" => ($lng=="en" ? "lang-selected" : "lang"),
                   "label" => "EN"
                   );
      $rs->addRec($rec);
     
      global $webPage;
      $webPage->addRecordset($rs);
    }

  /** Returns the name of the selected language */
  function get_lng_name()
    {
      $lng = $this->getSVar("selected");
      switch ($lng)
        {
        default:
        case "al":
          $lng_name = "Shqip";
          break;
        case "it":
          $lng_name = "Italiano";
          break;
        case "fr":
          $lng_name = "Francais";
          break;
        case "en":
          $lng_name = "English";
          break;
        }
      return $lng_name;
    }

  /** Returns the message in the selected language */
  function get_text($msg)
    {
      if (isset($this->messages[$msg]))
        {
          return $this->messages[$msg];
        }
      else
        {
          return $msg;
        }
    }
}
?>